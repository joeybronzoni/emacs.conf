;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(package-initialize)

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (load "~/.emacs.company-yasnippet.el") ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;;^^^^^^^for keybindings, icons, neotree, etc^^^^^^^^    ;;
    (load "~/.emacs.refactor-keybindings.el")             ;;
 ;;^^^^^^^ End for keybindings, icons, neotree, etc^^^^^^^;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; ;;=======for the awesome config from=====> https://github.com/foretagsplatsen/emacs-js      ;;
    (load "~/.emacs.refactor-flycheck.el")                                                      ;;
    (load "~/.emacs.refactor.el")                                                               ;;
 ;; ;;======= END for the awesome config from=====> https://github.com/foretagsplatsen/emacs-js ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; ;;------for the awesome config from----> https://github.com/phardyn/emacs ;;
    (load "~/.emacs.fortags.el")                                              ;;
 ;; ;;------ END for the awesome config from https://github.com/phardyn/emacs ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; ;;------for the awesome config from----> https://github.com/phardyn/emacs ;;
  (load "~/.emacs.xwidget_youtube.el")                                        ;;
 ;; ;;------ END for the awesome config from https://github.com/phardyn/emacs ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   (load "~/.jedi-starter.el") ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   (load "~/.emacs.python.el") ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; For stack trace on errors uncomment this ;;
;; (setq debug-on-error t)                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '.emacs)
;;; .emacs ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(material-theme better-defaults ztree yaml-mode xwidgete xref-js2 xkcd widgetjs web-mode use-package undo-tree tern-auto-complete syntax-subword smooth-scroll smex smartparens smart-forward scss-mode redo+ php-mode pdf-tools parinfer nyan-mode neotree move-text markdown-mode magit less-css-mode json-mode js-doc jedi indium ido-ubiquitous hlinum helm-projectile handlebars-mode gulp-task-runner god-mode flycheck exec-path-from-shell eslintd-fix elpy diminish csv-mode company-tern column-enforce-mode coffee-mode clojure-mode avy atom-one-dark-theme atom-dark-theme anaconda-mode amd-mode all-the-icons-ivy all-the-icons-gnus ac-js2)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
