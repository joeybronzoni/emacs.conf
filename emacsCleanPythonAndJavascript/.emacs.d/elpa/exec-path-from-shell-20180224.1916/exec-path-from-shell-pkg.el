;;; -*- no-byte-compile: t -*-
(define-package "exec-path-from-shell" "20180224.1916" "Get environment variables such as $PATH from the shell" 'nil :commit "4c67a95a65a0027bc28cef099e43aaa9776406b9" :url "https://github.com/purcell/exec-path-from-shell" :keywords '("unix" "environment"))
