;;; -*- no-byte-compile: t -*-
(define-package "helm-projectile" "20180319.1414" "Helm integration for Projectile" '((helm "1.7.7") (projectile "0.14.0") (cl-lib "0.3")) :commit "c59dfda92776290947e855798286f4b46418ec9a" :url "https://github.com/bbatsov/helm-projectile" :keywords '("project" "convenience"))
