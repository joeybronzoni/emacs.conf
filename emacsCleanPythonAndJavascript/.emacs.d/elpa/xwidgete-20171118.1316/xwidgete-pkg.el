;;; -*- no-byte-compile: t -*-
(define-package "xwidgete" "20171118.1316" "enhances usability of current xwidget browser" '((emacs "25")) :commit "e4e8410fe32176df85b46234717824519443fb04" :url "https://github.com/tuhdo/xwidgete" :keywords '("xwidgete" "tools"))
