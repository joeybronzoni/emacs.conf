
;;===react snippets from here https://github.com/johnmastro/react-snippets.el/tree/master/snippets/js-mode
  (eval-after-load 'js2-mode
  '(progn
     (require 'js2-imenu-extras)

     ;; The code to record the class is identical to that for
     ;; Backbone so we just make an alias
     (defalias 'js2-imenu-record-react-class
       'js2-imenu-record-backbone-extend)

     (unless (loop for entry in js2-imenu-extension-styles
		   thereis (eq (plist-get entry :framework) 'react))
       (push '(:framework react
			  :call-re "\\_<React\\.createClass\\s-*("
			  :recorder js2-imenu-record-react-class)
	     js2-imenu-extension-styles))

     (add-to-list 'js2-imenu-available-frameworks 'react)
     (add-to-list 'js2-imenu-enabled-frameworks 'react)))

 ;;===react snippets from here https://github.com/johnmastro/react-snippets.el/tree/master/snippets/js-mode




(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(avy-background t)
 '(avy-keys
   '(97 115 100 102 103 104 106 107 108 113 119 101 114 116 121 117 105 111 112 122 120 99 118 98 110 109))
 '(coffee-tab-width 2)
 '(flycheck-temp-prefix ".flycheck")
 '(global-flycheck-mode t)
 '(global-hl-line-mode t)
 '(global-linum-mode t)
 '(js-doc-description-line " * @desc
")
 '(js-indent-level 2)
 '(js2-global-externs
   '("module" "require" "setTimeout" "clearTimeout" "setInterval" "clearInterval" "location" "console" "JSON" "angular" "toastr" "$" "moment" "_"))
 '(js2-highlight-level 3)
 '(js2-mode-show-parse-errors nil)
 '(js2-mode-show-strict-warnings nil)
 '(linum-format " %2d ")
 '(mode-line-format
   '("%e "
     (:eval
      (cond
       ((buffer-modified-p)
        (propertize "✖ modified" 'face 'error))
       (t
        (propertize "✔ saved" 'face 'success))))
     " • "
     (:propertize "%02l" face
                  ((t
                    (:foreground "#C678DD" :weight normal))))
     ","
     (:propertize "%02c" face
                  ((t
                    (:foreground "#C678DD" :weight normal))))
     " • "
     (:eval
      (list
       (nyan-create)))
     " • " mode-name " • "
     (:propertize mode-line-buffer-identification face
                  ((t
                    (:foreground "#61AFEF" :weight normal))))
     mode-line-end-spaces))
 '(nyan-bar-length 16)
 '(nyan-mode t)
 '(package-selected-packages
   '(elpy ztree coffee-mode json-mode helm-flycheck exec-path-from-shell flycheck parinfer multiple-cursors smart-forward expand-region php-mode yasnippet yaml-mode xkcd web-mode use-package undo-tree syntax-subword smooth-scroll smex smartparens scss-mode redo+ nyan-mode move-text markdown-mode magit less-css-mode js-doc jedi ido-ubiquitous hlinum helm-projectile god-mode csv-mode column-enforce-mode clojure-mode avy atom-one-dark-theme atom-dark-theme anaconda-mode ac-js2))
 '(show-smartparens-global-mode t)
 '(size-indication-mode t)
 '(web-mode-code-indent-offset 2)
 '(web-mode-css-indent-offset 4)
 '(web-mode-enable-current-element-highlight t)
 '(web-mode-markup-indent-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ace-jump-face-background ((t (:background nil :foreground "#5C6370" :inverse-video nil))))
 '(ace-jump-face-foreground ((t (:background nil :foreground "#FFDD33" :inverse-video nil :weight bold))))
 '(avy-lead-face ((t (:foreground "#FFDD33" :weight normal))))
 '(avy-lead-face-0 ((t (:foreground "#E06C75" :weight normal))))
 '(avy-lead-face-1 ((t (:foreground "#7FFF00" :weight normal))))
 '(avy-lead-face-2 ((t (:foreground "#56B6C2" :weight normal))))
 '(flycheck-error ((t (:underline "#E06C75"))))
 '(flycheck-error-list-error ((t (:inherit error))))
 '(flycheck-info ((t (:underline "#528BFF"))))
 '(flycheck-warning ((t (:underline "#E5C07B"))))
 '(helm-match ((t (:foreground "#E06C75" :weight bold))))
 '(ido-first-match ((t (:foreground "#E06C75" :weight bold))))
 '(ido-only-match ((t (:foreground "#528BFF" :weight bold))))
 '(ido-subdir ((t (:foreground "#E5C07B"))))
 '(js2-error ((t (:underline "Red1"))))
 '(linum ((t (:stipple nil :background "#282C34" :distant-foreground "#5C6370" :foreground "#5C6370" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal))))
 '(linum-highlight-face ((t (:stipple nil :background "#2F343D" :distant-foreground "#5C6370" :foreground "#528BFF" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight bold))))
 '(mode-line ((t (:background "#323232" :foreground "#AAAAAA"))))
 '(mode-line-inactive ((t (:background "#444444" :foreground "#AAAAAA"))))
 '(sp-show-pair-match-face ((t (:foreground "#528BFF" :weight normal))))
 '(sp-show-pair-mismatch-face ((t (:foreground "#E06C75" :weight normal))))
 '(web-mode-current-element-highlight-face ((t (:inherit web-mode-variable-name-face)))))
