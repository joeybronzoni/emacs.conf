;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(package-initialize)

;; INSTALL PACKAGES
;; --------------------------------------

(require 'package)

(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

(elpy-enable)
(setq elpy-rpc-backend "jedi")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (add-to-list 'package-archives								    ;;
;;        '("melpa" . "http://melpa.org/packages/") t)						    ;;
;; 												    ;;
;; (package-initialize)										    ;;
;; (when (not package-archive-contents)								    ;;
;;   (package-refresh-contents))								    ;;
;; 												    ;;
;; ;; Load & Install the 3 packages								    ;;
;; (defvar pythonPackageList									    ;;
;;   '(better-defaults										    ;;
;;     ein ;; add the ein package (emacs ipython)						    ;;
;;     material-theme										    ;;
;;     elpy ;; add the elpy package								    ;;
;;     flycheck ;; add the flycheck package							    ;;
;;     py-autopep8 ;; add the autopep8								    ;;
;;     ))											    ;;
;; 												    ;;
;; (mapc #'(lambda (package)									    ;;
;;     (unless (package-installed-p package)							    ;;
;;       (package-install package)))								    ;;
;;       pythonPackageList)									    ;;
;; 												    ;;
;; ;; BASIC CUSTOMIZATION									    ;;
;; ;; --------------------------------------							    ;;
;; 												    ;;
;; (setq inhibit-startup-message t) ;; hide the startup message					    ;;
;; (load-theme 'material t) ;; load material theme						    ;;
;; (global-linum-mode t) ;; enable line numbers globally					    ;;
;; 												    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	    ;;
;; ;; ;;-----When you type C-c C-c to run your python shell                               ;;	    ;;
;; ;;        you might get an error about readline support, there are                     ;;	    ;;
;; ;;        two patches but the 2nd should work- here is the                             ;;	    ;;
;; ;;        1st                                                                          ;;	    ;;
;; ;;                                                                                     ;;	    ;;
;; ;; (with-eval-after-load 'python                                                       ;;	    ;;
;; ;;   (defun python-shell-completion-native-try ()                                      ;;	    ;;
;; ;;     "Return non-nil if can trigger native completion."                              ;;	    ;;
;; ;;     (let ((python-shell-completion-native-enable t)                                 ;;	    ;;
;; ;;           (python-shell-completion-native-output-timeout                            ;;	    ;;
;; ;;            python-shell-completion-native-try-output-timeout))                      ;;	    ;;
;; ;;       (python-shell-completion-native-get-completions                               ;;	    ;;
;; ;;        (get-buffer-process (current-buffer))                                        ;;	    ;;
;; ;;        nil "_"))))                                                                  ;;	    ;;
;; ;; ;;----------------------------------------------------                              ;;	    ;;
;; ;; ;;or this                                                                           ;;	    ;;
;;  (setq python-shell-completion-native-enable nil)                                      ;;	    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	    ;;
;; 												    ;;
;; ;; Now enable elpy										    ;;
;; (elpy-enable)										    ;;
;; 												    ;;
;; 												    ;;
;; ;; get Flycheck on the horn for syntax							    ;;
;; (when (require 'flycheck nil t)								    ;;
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))				    ;;
;;   (add-hook 'elpy-mode-hook 'flycheck-mode))							    ;;
;; 												    ;;
;; (require 'py-autopep8)									    ;;
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)					    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;				    ;;
;; ;; (setq python-shell-interpreter "jupyter"                       ;;				    ;;
;; ;;       python-shell-interpreter-args "console --simple-prompt") ;;				    ;;
;; ;; (setq ein:completion-backend 'ein:use-ac-jedi-backend)         ;;				    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;				    ;;
;; 												    ;;
;; 												    ;;
;; ;;(elpy-use-ipython)										    ;;
;; (setq python-shell-interpreter "ipython"							    ;;
;;       python-shell-interpreter-args "-i --simple-prompt")					    ;;
;; 												    ;;
;; 												    ;;
;;   (defun load-directory (dir)								    ;;
;;       (let ((load-it (lambda (f)								    ;;
;; 		       (load-file (concat (file-name-as-directory dir) f)))			    ;;
;; 		     ))										    ;;
;; 	(mapc load-it (directory-files dir nil "\\.el$"))))					    ;;
;; (load-directory "~/emacs_orig")								    ;;
;; (setq ein:completion-backend 'ein:use-ac-jedi-backend)					    ;;
;; (provide '.emacs)										    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; .emacs.python.el ends here
