


;;***************************For the awesome emacs-js all-in-one package************************************8
(package-initialize)
(require 'package)
(require 'widgetjs) 
(require 'amd-mode)
(require 'gulp-task-runner)
(require 'xref-js2)
(require 'indium) 
(load "~/.emacs.d/emacs-js/emacs-js.el")
 (add-to-list 'auto-mode-alist '("\\.json$" . emacs-js-mode))
;;***************************For the awesome emacs-js all-in-one package************************************8
