;;; react-snippets-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "react-snippets" "react-snippets.el" (22989
;;;;;;  49105 214578 685000))
;;; Generated autoloads from react-snippets.el

(autoload 'react-snippets-initialize "react-snippets" "\


\(fn)" nil nil)

(eval-after-load 'yasnippet '(react-snippets-initialize))

;;;***

;;;### (autoloads nil nil ("react-snippets-pkg.el") (22989 49105
;;;;;;  214578 685000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; react-snippets-autoloads.el ends here
