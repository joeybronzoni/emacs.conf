;;; -*- no-byte-compile: t -*-
(define-package "pug-mode" "20171018.829" "Major mode for jade/pug template files" '((emacs "24.4") (cl-lib "0.5")) :commit "9dcebdb10c50620a3a5c6d71361d53bf482a482e" :url "https://github.com/hlissner/emacs-pug-mode" :keywords '("markup" "language" "jade" "pug"))
