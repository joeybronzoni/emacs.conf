;;; eslintd-fix-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "eslintd-fix" "eslintd-fix.el" (23081 41828
;;;;;;  819166 892000))
;;; Generated autoloads from eslintd-fix.el

(autoload 'eslintd-fix-mode "eslintd-fix" "\
Use eslint_d to automatically fix javascript before saving.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("eslintd-fix-pkg.el") (23081 41828 565833
;;;;;;  557000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; eslintd-fix-autoloads.el ends here
