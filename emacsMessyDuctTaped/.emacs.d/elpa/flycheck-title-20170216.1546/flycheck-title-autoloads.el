;;; flycheck-title-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "flycheck-title" "flycheck-title.el" (22856
;;;;;;  31522 805027 965000))
;;; Generated autoloads from flycheck-title.el

(defvar flycheck-title-mode nil "\
Non-nil if Flycheck-Title mode is enabled.
See the command `flycheck-title-mode' for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `flycheck-title-mode'.")

(custom-autoload 'flycheck-title-mode "flycheck-title" nil)

(autoload 'flycheck-title-mode "flycheck-title" "\
Global minor mode for showing flycheck errors in the frame title.

\(fn &optional ARG)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; flycheck-title-autoloads.el ends here
