;;; json-snatcher-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "json-snatcher" "json-snatcher.el" (22856 32076
;;;;;;  719377 742000))
;;; Generated autoloads from json-snatcher.el

(autoload 'jsons-print-path "json-snatcher" "\
Print the path to the JSON value under point, and save it in the kill ring.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("json-snatcher-pkg.el") (22856 32076 779110
;;;;;;  651000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; json-snatcher-autoloads.el ends here
