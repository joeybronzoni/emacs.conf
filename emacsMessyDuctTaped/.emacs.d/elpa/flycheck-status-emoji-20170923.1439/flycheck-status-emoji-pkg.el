;;; -*- no-byte-compile: t -*-
(define-package "flycheck-status-emoji" "20170923.1439" "Show flycheck status using cute, compact emoji" '((cl-lib "0.1") (emacs "24") (flycheck "0.20") (let-alist "1.0")) :commit "e85072a04f4277a48d90364b8e48fcb26a650d38" :url "https://github.com/liblit/flycheck-status-emoji" :keywords '("convenience" "languages" "tools"))
