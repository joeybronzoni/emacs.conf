;;; web-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "web-mode" "web-mode.el" (23081 41791 985833
;;;;;;  259000))
;;; Generated autoloads from web-mode.el

(autoload 'web-mode "web-mode" "\
Major mode for editing web templates.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("web-mode-pkg.el") (23081 41791 55833
;;;;;;  251000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; web-mode-autoloads.el ends here
