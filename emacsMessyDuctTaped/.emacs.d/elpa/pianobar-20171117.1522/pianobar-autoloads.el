;;; pianobar-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "pianobar" "pianobar.el" (23081 41801 275833
;;;;;;  335000))
;;; Generated autoloads from pianobar.el

(autoload 'pianobar "pianobar" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("pianobar-pkg.el") (23081 41801 112499
;;;;;;  999000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; pianobar-autoloads.el ends here
