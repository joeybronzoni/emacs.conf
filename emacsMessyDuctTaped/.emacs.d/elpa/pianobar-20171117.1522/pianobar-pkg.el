;;; -*- no-byte-compile: t -*-
(define-package "pianobar" "20171117.1522" "thin wrapper for Pianobar, a Pandora Radio client" 'nil :commit "68fe0ed839f6775535081b3ae0a946ccaf11234a" :url "http://github.com/agrif/pianobar.el")
