(add-hook 'php-mode-hook '(lambda ()
                           (auto-complete-mode t)
                           (require 'ac-php)
                           (setq ac-sources  '(ac-source-php ) )
                           (yas-global-mode 1)

                           (define-key php-mode-map  (kbd "C-]") 'ac-php-find-symbol-at-point)   ;goto define
                           (define-key php-mode-map  (kbd "C-t") 'ac-php-location-stack-back   ) ;go back
                           ))



(setq package-archives
     '(("melpa" . "https://melpa.org/packages/")) )

(package-initialize)
(unless (package-installed-p 'ac-php )
 (package-refresh-contents)
 (package-install 'ac-php )
 )
(require 'cl)
(require 'php-mode)
(add-hook 'php-mode-hook '(lambda ()
                           (auto-complete-mode t)
                           (require 'ac-php)
                           (setq ac-sources  '(ac-source-php ) )
                           (yas-global-mode 1)

                           (define-key php-mode-map  (kbd "C-]") 'ac-php-find-symbol-at-point)   ;goto define
                           (define-key php-mode-map  (kbd "C-t") 'ac-php-location-stack-back   ) ;go back
                           ))

;added this for yasnippets
(require 'php-auto-yasnippets)
(require 'php-auto-yasnippets)
;(setq php-auto-yasnippet-php-program "~/path/to/Create-PHP-YASnippet.php")
