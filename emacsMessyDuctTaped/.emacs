


 (require 'package) 
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;
(require 'package) (package-initialize)
;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;

;;----menu-bar-open f10 doesnt work------------;
 (global-set-key (kbd "C-c C-&") (quote menu-bar-open))
 (global-set-key [3 38] (quote menu-bar-open))
;;----menu-bar-open f10 doesnt work------------;

;;----menu-bar-open f10 doesnt work------------;
 (global-set-key (kbd "C-c C-b") (quote buffer-menu))
;;(global-set-key [3 38] (quote buffer-menu))
;;----menu-bar-open f10 doesnt work------------;

;;----yas-activate-extra-mode------------;
 (global-set-key (kbd "C-c C-y") (quote yas-activate-extra-mode))

;;----auto-complete-mode------------;
 (global-set-key (kbd "C-x C-a") (quote auto-complete-mode))
;; (global-set-key (kbd "C-c C-a") (quote auto-complete-mode))
;;----ac-complete-tern-complete------------;
;; (global-set-key (kbd "C-c C-w") (quote ac-complete-tern-completion))

;;----neotree------------------------------------------------------------------;;
;; (global-set-key [3 14] (quote neotree-toggle))
 (global-set-key (kbd "C-c C-^") (quote neotree-toggle))
 (add-to-list 'load-path "/directory/containing/neotree/")
;;(require 'neotree)
 (global-set-key [f8] 'neotree-toggle)
;;  (setq neo-smart-open t)
;;---endneotree----------------------------------------------------------------;;

;;-----pianobar--------------------------------------------------;;
 (add-to-list 'load-path "/home/agrif/emacsinclude")
   (autoload 'pianobar "pianobar" nil )
;;-----pianobar--------------------------------------------------;;
;;-------handlebars mode--------------------------------------;;
   (require 'handlebars-mode)
;;-------handlebars mode--------------------------------------;;

;;------------line-mode and macro for comment and indent----------------------------------;; 
  (add-hook 'prog-mode-hook 'linum-mode)
 (fset 'indent-buffer
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("< >xindent-region" 0 "%d")) arg)))
 ; global-set-key
 ; repeat-complex-command
 (global-set-key [27 80] (quote indent-buffer))
(global-set-key [3 9] (quote indent-buffer)) ; ctl-c ctl-i
(global-set-key [3 3] (quote comment-region)); ctl-c ctl-c
(global-set-key [3 21] (quote uncomment-region)) ;; ctl-c ctl-u
(ido-mode 1)
(global-linum-mode)
(setq linum-format "%4d \u2502 ")
;; Show-hide
(global-set-key (kbd "") 'hs-show-block)
(global-set-key (kbd "") 'hs-show-all)
(global-set-key (kbd "") 'hs-hide-block)
(global-set-key (kbd "") 'hs-hide-all)



(cond ((< emacs-major-version 24)
       (setq custom-file "~/.emacs.react.el"))
        (t (setq custom-file "~/.emacs.react.el")))
;;http://www.flycheck.org/manual/latest/index.html
;;No one likes to type a full yes, y is enough as a confirmation.
(defalias 'yes-or-no-p 'y-or-n-p)
;;For easily identification of the current line.
;;(global-hl-line-mode)
;;;this works------------------------------------------------
(require 'smartparens-config)(require 'js)
(add-hook 'js-mode-hook #'smartparens-mode)
;;  (require 'cc-mode)not working-but it still matches the {} and []
 ;;(define-key js-mode-map "<" 'paredit-open-curly)
;; (define-key js-mode-map ">" 'paredit-close-curly-and-newline)
(smartparens-global-mode)
 ;; use web-mode for .jsx files
 (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
 ;;====https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.jsx$" . js-mode))
 (add-hook 'js-mode-hook 'js2-minor-mode)
 (add-hook 'js2-mode-hook 'ac-js2-mode)
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 ;;=end of https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
;; use web-mode for .jsx files
 (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
 ;;====https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.jsx$" . js-mode))
 (add-hook 'js-mode-hook 'js2-minor-mode)
 (add-hook 'js2-mode-hook 'ac-js2-mode)
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 ;;=end of https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . web-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . flow-jsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . jsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . flow-jsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . jsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("components\\/.*\\.jsx\\'" . js2-mode))
;;added for php
(add-to-list 'auto-mode-alist '("components\\/.*\\.php\\'" . php-mode))
 ;; use web-mode for .jsx files
 (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
 ;;====https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.jsx$" . js-mode))
 (add-hook 'js-mode-hook 'js2-minor-mode)
 (add-hook 'js2-mode-hook 'ac-js2-mode)
 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
 (add-to-list 'auto-mode-alist '("\\.js$" . js-mode))
 ;;=end of https://truongtx.me/2014/02/23/set-up-javascript-development-environment-in-emacs===;;
(with-eval-after-load 'rjsx
  (define-key rjsx-mode-map "<" nil)
    (define-key rjsx-mode-map (kbd "C-d") nil))
(add-to-list 'auto-mode-alist '("\\.jsx$" . javascript-mode))
(add-to-list 'auto-mode-alist '("\\.jsx$" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.jsx$" . js2-mode))
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
      (let ((web-mode-enable-part-face nil))
	ad-do-it)
        ad-do-it))
(defvar tern-ac-js-major-modes '(js2-mode js2-jsx-mode js-mode javascript-mode flow-jsx-mode))
(use-package js2-mode) ;;dont need this I think
(require 'flymake-jslint)
(require 'flymake-cursor)
;;(require 'yasnippet)
;;(require 'auto-complete-config)
;;(require 'tern)
;;;;;---------------------------------myFolders-------------------------------------------
;;(load "~/.emacs.yasnippets.el")
 (require 'auto-complete ) 
(load "~/.emacs.autoComplete.el")
  (load "~/.emacs.linter.el")
  (load "~/.emac.flowJsx.el")
;;this is from the emacs python tutorial for jedi python


;; (setq ein:completion-backend 'ein:use-ac-jedi-backend)
;; (package-initialize)
;; (require 'ein)
;; (require 'ein-loaddefs)
;; (require 'ein-notebook)
;; (require 'ein-subpackages)
;; (load "~/.jedi_starter.el")
;; (elpy-use-ipython)
;;(load "~/.emacs.flycheck.el")
;;(load "~/.emacs.webmode.el")
;;(load "~/.emacs.tern.el")
;;(load "~/.emacs.flowjsx.el")
;;;---------------------------------myFolders-------------------------------------------
(use-package smartparens-config :ensure nil)
(sp-use-smartparens-bindings)
(sp-pair "(" nil :post-handlers '(("| " "SPC")))
(sp-pair "[" nil :post-handlers '(("| " "SPC")))
(sp-pair "{" nil :post-handlers '(("| " "SPC")))
(add-hook 'ruby-mode-hook
		       (lambda ()
			 (sp-local-pair 'ruby-mode "{" nil :post-handlers '(("| " "SPC")))))
	     (add-hook 'nxml-mode-hook
		       (lambda ()
			 (sp-local-pair 'nxml-mode "<" ">" :actions :rem)))
	     (add-hook 'web-mode-hook
		       (lambda ()
			 (sp-local-pair 'web-mode "<" nil :actions :rem)
			 (sp-local-pair 'web-mode "<%" "%>" :post-handlers '(("| " "SPC") (" | " "=")))))
	     ;; Do not escape closing pair in string interpolation
	     (add-hook 'swift-mode-hook
		       (lambda ()
			 (sp-local-pair 'swift-mode "\\(" nil :actions :rem)
			 (sp-local-pair 'swift-mode "\\(" ")")))

	     (set-face-attribute 'sp-show-pair-match-face nil
				 :foreground "#F169AB")
	       (show-smartparens-global-mode)


  (require 'web-mode)
  (require 'js2-mode)

 (load "~/.emacs.yasnippets.el")
;;for the emacs mess--------------------------------------------------------------
 ;;  (load "~/.emacs.webMode.el")
 ;;  (load "~/.emacs.react.el")
 ;;  (load "~/.emacs.linter.el")
 ;; (load "~/.eslintrc" )























































;;;------------------------------------You can then run a Javascript REPL with M-x run-js and also send portions of files by selecting them and then running M-x send-region. Neat.---------------------------------------------------------------
;; You can add the following lines to your .emacs to take advantage of
;;   cool keybindings for sending things to the javascript interpreter inside
;;   of Steve Yegge's most excellent js2-mode.
   (add-hook 'js2-mode-hook
             (lambda ()
               (local-set-key (kbd "C-x C-e") 'js-send-last-sexp)
               (local-set-key (kbd "C-c b") 'js-send-buffer)
               ))

 
(add-to-list 'load-path "~/path/to/js-comint")
(require 'js-comint)
;; Use node as our repl
(setq inferior-js-program-command "node")

;;  (setq inferior-js-mode-hook
;;        (lambda ()
;; ;; 	;; We like nice colors
;;  	(ansi-color-for-comint-mode-on)
;; ;; 	;; Deal with some prompt nonsense
;;  	(add-to-list 'comint-preoutput-filter-functions
;;  		     (lambda (output)
;;  		       (replace-regexp-in-string ".*1G\.\.\..*5G" "..."
;;  						                      (replace-regexp-in-string ".*1G.*3G" "&gt;" output))))
 
;;;------------------------------------javascript REPL--------------------------------------------------------------
;;;for the emacs mess--------------------------------------------------------------
;; ;;(add-to-list 'load-path "~/emacsMess/")
;; (require 'flycheck)
;; (require 'exec-path-from-shell) ;; if not using the ELPA package
;; (require 'flymake-jshint)
;; (require 'flymake-jslint)
;; (require 'flycheck-flow)
;; (require 'flycheck-demjsonlint)
;; (require 'web-mode)
;; (require 'web-beautify) ;; Not necessary if using ELPA package
;; (require 'babel)
;; (require 'exec-path-from-shell) ;; if not using the ELPA package
;; (require 'yasnippet)
;; (require 'auto-complete-config)
;; ;; (load "~/.emacs.flycheck.el")

;; ;; (load "~/.emacs.js2.el")
;; ;; (load "~/.emacs.js3.el")
;; ;; (load "~/.emacs.jsx.el")
;; ;;====exec-path-from-shel=====================================;;
;;  (when (memq window-system '(mac ns))
;;         (exec-path-from-shell-initialize))
;;  (require 'exec-path-from-shell) ;; if not using the ELPA package
;;       (exec-path-from-shell-initialize)
;; ;; ;;---end-exec-path-from-shel---------------------------------;;
(setq ein:completion-backend 'ein:use-ac-jedi-backend)
(package-initialize)
(require 'ein)
(require 'ein-loaddefs)
(require 'ein-notebook)
(require 'ein-subpackages)
(load "~/.jedi_starter.el")
(elpy-use-ipython)
