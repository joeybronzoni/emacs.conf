
; (add-to-list 'load-path "~/path/to/lintnode")
;;(require 'flymake-jslint)
;; Make sure we can find the lintnode executable
					

;i comment this out					
;(setq lintnode-location "~/path/to/lintnode")
;; JSLint can be... opinionated
 (setq lintnode-jslint-excludes (list 'nomen 'undef 'plusplus 'onevar 'white))
;; Start the server when we first open a js file and start checking

 (add-hook 'js-mode-hook
 	  (lambda ()
	                (lintnode-hook)))

 (add-to-list 'load-path "~/emacs/minor-modes")
;; Nice Flymake minibuffer messages
;;(require 'flymake-cursor)


  (add-hook 'js-mode-hook
	  (lambda ()
	    ;; Scan the file for nested code blocks
	    (imenu-add-menubar-index)
	    ;; Activate the folding mode
	    (hs-minor-mode t)))

;; how to get this to work line 26 - 47 js2mode--------------------------------------------------------------------------------------------------------------------
 (global-flycheck-mode)
 (flycheck-add-mode 'javascript-eslint 'web-mode)


    (use-package js2-mode
	     :mode ("\\.js$" "\\.json")
	     :config
	     (setq js2-mode-show-parse-errors nil
		   js2-mode-show-strict-warnings nil
		   js2-mode-assume-strict t
		   js2-basic-offset 2
		   js2-bounce-indent-p t)

	     ;; Highlight `require()' as a buildtin javascript keyword
	     (font-lock-add-keywords 'js2-mode
				     '(("\\(require\\)(.*)" . (1 font-lock-keyword-face))))

	     (set-face-attribute 'js2-function-param nil
				 :foreground nil
				 :inherit 'font-lock-constant-face)

	     (use-package js-comint))
  (use-package rjsx-mode
	     :mode "\\.jsx\\'"
	     :config
	       (flycheck-add-mode 'javascript-eslint 'rjsx-mode))
;; how to get this to work line 26 - --------------------------------------------------------------------------------------------------------------------

;; CSS Mode
(use-package css-mode
	     :ensure nil
	     :mode "\\'.css\\'"
	     :config
	     (setq css-indent-offset 2)

	     (add-hook 'css-mode-hook
		       (lambda () (subword-mode -1))))



 (flycheck-define-checker jsxhint-checker
  "A JSX syntax and style checker based on JSXHint."

  :command ("jsxhint" source)
  :error-patterns
  ((error line-start (1+ nonl) ": line " line ", col " column ", " (message) line-end))
  :modes (web-mode))
(add-hook 'web-mode-hook
	  (lambda ()
	    (when (equal web-mode-content-type "jsx")
	      ;; enable flycheck
	      (flycheck-select-checker 'jsxhint-checker)
	                    (flycheck-mode))))


;;;jsxhint checker need to register for flow-jsx-mode ---how???
 ;; (flycheck-def-config-file-var flycheck-jscs
 ;;     javascript-jscs
 ;;     ".jscs"
 ;;   :safe #'stringp)

 ;; (flycheck-define-checker javascript-jscs
 ;;   "A jscs code style checker."
 ;;   :command ("jscs" "--reporter" "checkstyle"
 ;;          (config-file "--config" flycheck-jscs) source)
 ;;   :error-parser flycheck-parse-checkstyle
 ;;   :modes (js-mode js2-mode js3-mode flow-jsx-mode)
 ;;   :next-checkers (javascript-jshint))
 ;; (provide 'flycheck-jscs)
 ;; ;;+++++from here up+++++_+_+_+_+_+_+_+__+_;;=================================;;
;;;jsxhint checker need to register for flow-jsx-mode ---how???
