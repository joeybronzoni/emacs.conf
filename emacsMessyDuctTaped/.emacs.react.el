
;;===react snippets from here https://github.com/johnmastro/react-snippets.el/tree/master/snippets/js-mode
  (eval-after-load 'js2-mode
  '(progn
     (require 'js2-imenu-extras)

     ;; The code to record the class is identical to that for
     ;; Backbone so we just make an alias
     (defalias 'js2-imenu-record-react-class
       'js2-imenu-record-backbone-extend)

     (unless (loop for entry in js2-imenu-extension-styles
		   thereis (eq (plist-get entry :framework) 'react))
       (push '(:framework react
			  :call-re "\\_<React\\.createClass\\s-*("
			  :recorder js2-imenu-record-react-class)
	     js2-imenu-extension-styles))

     (add-to-list 'js2-imenu-available-frameworks 'react)
     (add-to-list 'js2-imenu-enabled-frameworks 'react)))

 ;;===react snippets from here https://github.com/johnmastro/react-snippets.el/tree/master/snippets/js-mode




(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-start nil)
 '(ac-trigger-key "TAB")
 '(ac-use-menu-map t)
 '(package-selected-packages
   (quote
    (ein virtualenvwrapper php+-mode web-mode-edit-element use-package tern-auto-complete smartparens rjsx-mode react-snippets pug-mode pianobar php-auto-yasnippets neotree magit jsx-mode json-mode js3-mode js2-refactor js-comint js-auto-beautify handlebars-mode flymake-php flymake-jslint flymake-jshint flymake-cursor flycheck-title flycheck-status-emoji flycheck-pos-tip flycheck-flow flycheck-demjsonlint flycheck-color-mode-line exec-path-from-shell eslintd-fix elpy company-tern cider babel ac-php ac-js2))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
