

 ;;-----New from here from this link https://truongtx.me/2014/02/23/set-up-javascript----;;
 ;;-----development-environment-in-emacs-------------------------------------------------;;
 (require 'web-mode)
 (add-to-list 'load-path "~/.emacs.d/lisp/")
 (add-to-list 'load-path 'inherit)
 (setq-default flycheck-emacs-lisp-load-path 'inherit)
 ;; http://www.flycheck.org/manual/latest/index.html
;; (add-to-list 'auto-mode-alist '("\\.js\\'" . flow-jsx-mode) 

;;====web-beautify=========================================;;
  (require 'web-beautify) ;; Not necessary if using ELPA package
  (eval-after-load 'js2-mode
    '(define-key js2-mode-map (kbd "C-c b") 'web-beautify-js))
  (eval-after-load 'json-mode
    '(define-key json-mode-map (kbd "C-c b") 'web-beautify-js))
  (eval-after-load 'sgml-mode
    '(define-key html-mode-map (kbd "C-c b") 'web-beautify-html))
  (eval-after-load 'css-mode
    '(define-key css-mode-map (kbd "C-c b") 'web-beautify-css))
 ;;----end web-beautify-------------------------------------;;
;;====exec-path-from-shel=====================================;;
 (when (memq window-system '(mac ns))
        (exec-path-from-shell-initialize))
 (require 'exec-path-from-shell) ;; if not using the ELPA package
      (exec-path-from-shell-initialize)
 ;;---end-exec-path-from-shel---------------------------------;;
 ;;===jsxmode======https://truongtx.me/2014/03/10/emacs-setup-jsx-mode-and-jsx-syntax-checking=========;;
 (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
 (defadvice web-mode-highlight-part (around tweak-jsx activate)
   (if (equal web-mode-content-type "jsx")
       (let ((web-mode-enable-part-face nil))
         ad-do-it)
         ad-do-it))
 ;;----end-jsxmode------https://truongtx.me/2014/03/10/emacs-setup-jsx-mode-and-jsx-syntax-checking----;;
 ;;========tern package and tern-auto-complete================== -;;
 (add-hook 'js-mode-hook (lambda () (tern-mode t)))
 (eval-after-load 'tern
   '(progn
      (require 'tern-auto-complete)
            (tern-ac-setup))) 
 ;;-----tern package and tern-auto-complete----------------------;;
 ;;=====tern Sometimes when you have just added .tern-project file or edit the file but Tern does not auto reload, you need to manually kill Tern server. This little piece of code does the trick=;
 ;; (defun delete-tern-process ()
 ;;   (interactive)
 ;;   (delete-process "Tern"))
 ;;When you encounter that problem, just activate the command delete-tern-process via M-x and then continue using as normal. Tern server will be launched automatically.
 ;;--------------tern------------------------------;;




;;;;-----addded this need to add auto-complete manually in modes-------------------------------------------
(require 'auto-complete-config)
(ac-config-default)
(set-face-background 'popup-tip-face "#272727")
(set-face-foreground 'popup-tip-face "#cfcab5")
(set-face-background 'popup-face "#cfcab5")
(set-face-background 'popup-scroll-bar-background-face "#cfcab5")
(set-face-background 'popup-scroll-bar-foreground-face "#272727")
(set-face-background 'popup-menu-mouse-face "#272727")
(set-face-background 'ac-selection-face "#272727")

(add-to-list 'load-path "~/.emacs.d/lisp/plugins/tern/emacs")
(setenv "NODE_PATH" "C:/Users/sesa369698/AppData/Roaming/npm/node_modules/tern")
(add-to-list 'load-path "~/.emacs.d/lisp/plugins/tern/")
(autoload 'tern-mode "tern.el" nil t)
(add-hook 'js-mode-hook (lambda () (tern-mode t)))
(add-hook 'js2-mode-hook (lambda () (tern-mode t)))
(add-to-list 'auto-mode-alist '("\\.less\\'" . css-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . css-mode))
					;(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.tmpl\\'" . html-mode))
(add-hook 'html-mode-hook
	  (lambda ()
	    ;; Default indentation is usually 2 spaces, changing to 4.
	    (set (make-local-variable 'sgml-basic-offset) 4)))

(eval-after-load 'tern
  '(progn
     (require 'tern-auto-complete)
     (tern-ac-setup)))
(defun delete-tern-process ()
  (interactive)
  (delete-process "Tern"))
(setf callback (lambda (port err)
		 (if port
		     (unwind-protect
			 (tern-req port doc runner)
		       (funcall runner '((err connection-failed)) nil))
		   (funcall f err nil))))
 ;;;;-----addded this need to add auto-complete manually in modes-------------------------------------------
