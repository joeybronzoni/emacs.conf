;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:
;; ;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;; ;; use web-mode for .jsx files
;; (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))

;; http://www.flycheck.org/manual/latest/index.html
(require 'flycheck)

;; turn on flychecking globally
(add-hook 'after-init-hook #'global-flycheck-mode)

;; disable jshint since we prefer eslint checking
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(javascript-jshint)))

;; use eslint with web-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'web-mode)

;; customize flycheck temp file prefix
(setq-default flycheck-temp-prefix ".flycheck")

;; disable json-jsonlist checking for json files
(setq-default flycheck-disabled-checkers
  (append flycheck-disabled-checkers
    '(json-jsonlist)))

;; https://github.com/purcell/exec-path-from-shell
;; only need exec-path-from-shell on OSX
;; this hopefully sets up path and other vars better
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))
;; ;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++




;; ;;===================================================================
;; ;; use local eslint from node_modules before global
;; ;; http://emacs.stackexchange.com/questions/21205/flycheck-with-file-relative-eslint-executable
;; (defun my/use-eslint-from-node-modules ()
;;   (let* ((root (locate-dominating-file
;;                 (or (buffer-file-name) default-directory)
;;                 "node_modules"))
;;          (eslint (and root
;;                       (expand-file-name "node_modules/eslint/bin/eslint.js"
;;                                         root))))
;;     (when (and eslint (file-executable-p eslint))
;;       (setq-local flycheck-javascript-eslint-executable eslint))))
;; (add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)
;; ;;===================================================================




;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; for better jsx syntax-highlighting in web-mode
;; - courtesy of Patrick @halbtuerke
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
    (let ((web-mode-enable-part-face nil))
      ad-do-it)
    ad-do-it))
;;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;; ;;-------------------------------------------------------------------
;; (add-to-list 'auto-mode-alist '("\\.jsx\\'" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
(global-flycheck-mode)
(flycheck-add-mode 'javascript-eslint 'web-mode)
;; ;;-------------------------------------------------------------------





;;^^^^^^^^^^^Syntax Highlighting^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
;; (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.js$" . web-mode))
;; (add-to-list 'auto-mode-alist '("\\.json$" . web-mode))
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
      (let ((web-mode-enable-part-face nil))
        ad-do-it)
    ad-do-it))
;; if want to use jshint instead of eslint
(flycheck-define-checker jsxhint-checker
  "A JSX syntax and style checker based on JSXHint."

  :command ("jsxhint" source)
  :error-patterns
  ((error line-start (1+ nonl) ": line " line ", col " column ", " (message) line-end))
  :modes (web-mode))
(add-hook 'web-mode-hook
          (lambda ()
            (when (equal web-mode-content-type "jsx")
              ;; enable flycheck
              (flycheck-select-checker 'jsxhint-checker)
              (flycheck-mode))))


;; ;;^^^^^^^^^^^Syntax Highlighting end^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



;; ;;::::::::js2-mode, js2-refactor, tern, tern-autocomplete::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
;;  (require 'js2-refactor)
;;      (add-hook 'js2-mode-hook #'js2-refactor-mode)
;; (js2r-add-keybindings-with-prefix "C-c C-m")
;;  (require 'js2-refactor)
;; (add-hook 'js-mode-hook #'js2-refactor-mode)
;; (js2r-add-keybindings-with-prefix "C-c C-m")
;;  (require 'js2-refactor)
;; (add-hook 'rjsx-mode-hook #'js2-refactor-mode)
;; (js2r-add-keybindings-with-prefix "C-c C-m")
;;  (require 'js2-refactor)
;; (add-hook 'js2-mode-hook #'js2-refactor-mode)
;;      (js2r-add-keybindings-with-prefix "C-c C-m")
(js2r-add-keybindings-with-prefix "C-c C-m")
(add-to-list 'auto-mode-alist '("\\.js$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.rjsx$" . js2-refactor-mode))

;; (add-to-list 'load-path " ~/.emacs.d/elpa/tern-20170925.1333/")
;; (autoload 'tern-mode "tern.el" nil t)
;; (add-hook 'js-mode-hook (lambda () (tern-mode t)))
;; (eval-after-load 'tern
;;    '(progn
;;       (require 'tern-auto-complete)
;;       (tern-ac-setup)))
;; (add-hook 'js2-mode-hook (lambda () (tern-mode t)))
;; (eval-after-load 'tern
;;    '(progn
;;       (require 'tern-auto-complete)
;;       (tern-ac-setup)))
;; (add-hook 'rjsx-mode-hook (lambda () (tern-mode t)))
;; (eval-after-load 'tern
;;    '(progn
;;       (require 'tern-auto-complete)
;;       (tern-ac-setup)))
;; (add-hook 'js-mode-hook (lambda () (tern-mode t)))
;; (eval-after-load 'tern
;;    '(progn
;;       (require 'tern-auto-complete)
;;       (tern-ac-setup)))
;; (eval-after-load 'tern
;;    '(progn
;;       (require 'tern-auto-complete)
;;       (tern-ac-setup)))

;; (defun delete-tern-process ()
;;   (interactive)
;;   (delete-process "Tern"))
;; ;;::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::




;; ;;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Set upjavscript development@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

 (add-to-list 'auto-mode-alist '("\\.json$" . js-mode))
;; ;;@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@Set upjavscript development@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;--------for the other linters---------------------------                                    ;;
;; ;;^^^^^^^^^^^Syntax Highlighting^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ;;
;; ;; (add-to-list 'auto-mode-alist '("\\.jsx$" . web-mode))                                      ;;
;; ;; (add-to-list 'auto-mode-alist '("\\.js$" . web-mode))                                       ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (add-to-list 'auto-mode-alist '("\\.json$" . web-mode))
(add-to-list 'flycheck-checkers 'jsxhint-checker)
(flycheck-add-mode 'jsxhint-checker 'web-mode)
(defadvice web-mode-highlight-part (around tweak-jsx activate)
  (if (equal web-mode-content-type "jsx")
      (let ((web-mode-enable-part-face nil))
        ad-do-it)
    ad-do-it))
;; if want to use jshint instead of eslint
(flycheck-define-checker jsxhint-checker
  "A JSX syntax and style checker based on JSXHint."

  :command ("jsxhint" source)
  :error-patterns
  ((error line-start (1+ nonl) ": line " line ", col " column ", " (message) line-end))
  :modes (web-mode))
(add-hook 'web-mode-hook
          (lambda ()
            (when (equal web-mode-content-type "jsx")
              ;; enable flycheck
              (flycheck-select-checker 'jsxhint-checker)
              (flycheck-mode))))
;; use eslint with web-mode for jsx files
(flycheck-add-mode 'javascript-eslint 'web-mode)



;; use local eslint from node_modules before global
;; http://emacs.stackexchange.com/questions/21205/flycheck-with-file-relative-eslint-executable
(defun my/use-eslint-from-node-modules ()
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      ;; (expand-file-name "node_modules/eslint/bin/eslint.js"
					  (expand-file-name "${NVM_BIN}/../lib/node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))
(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)

;; https://standardjs.com
;; * Note: when using javascript-standard, install sudo npm i-g standard, then add test script and ignore files
;  "scripts": {
;    "test": "standard && node myFile.js",
;    "start": "node server.js"
;  },

;; Add files to ignore
;"standard": {
;	"ignore": [
;	   "**/out/",
;	   "/lib/select2/",
;	   "/lib/ckeditor/",
;	   "tmp.js"
;	 ]
;   }

;; Variable is not defined? GLobal namespace issues? try adding this definition:
;{
;  "standard": {
;    "globals": [ "myVar1", "myVar2" ]
;  }
;}

;; ES6 features: npm i babel-eslint -D
;{
;  "standard": {
;    "parser": "babel-eslint"
;  }
;}

;; Typescript:
; npm install @typescript-eslint/parser @typescript-eslint/eslint-plugin --save-dev
; then
; standard --parser @typescript-eslint/parser --plugin typescript *.ts
;OR add this to package.json THEN YOU COULD RUN-> standard *.ts

;{
;  "standard": {
;    "parser": "@typescript-eslint/parser",
;    "plugins": [ "@typescript-eslint/eslint-plugin" ]
;  }
;}

(provide '.emacs.refactor-flycheck)
;;; .emacs.refactor-flycheck.el ends here
