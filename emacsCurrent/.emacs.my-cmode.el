(require 'package)

(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))

(package-initialize)

(require 'auto-complete)

(require 'auto-complete-config)
(ac-config-default)

(require 'yasnippet)
(yas-global-mode 1)

(defun my:ac-c-header-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers)
  (add-to-list 'achead:include-directories '"/usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/x86_64-pc-linux-gnu			     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/backward						     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include														     ;;
    /usr/local/include																					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed												     ;;
    /usr/include")
  )

(add-hook 'c++-mode-hook 'my:ac-c-header-init)
(add-hook 'c-mode-hook 'my:ac-c-header-init)

(define-key global-map (kbd "C-c ;") 'iedit-mode)

(semantic-mode 1)

(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic)
  )
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)

(global-ede-mode 1)
;; create project for our program
(ede-cpp-root-project "TEst"
		      :file "/home/arcarius/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp"
		      :include-path '("/../my_inc/"))
;;you can use system-include-path for setting up the system header file locations
