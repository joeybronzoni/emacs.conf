;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

 (require 'package)
  (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
    (setq load-path (cons (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/")
			  load-path))
    (setq load-path (cons (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/")
			  load-path))

(add-to-list 'load-path (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/"))
;;move backup copies to a different dir
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-directory-alist `((".tern-port" . "~/.saves")))
(setq backup-directory-alist `((".#*" . "~/.saves")))
(setq backup-directory-alist `((". #*" . "~/.saves")))
(setq backup-directory-alist `((" #*" . "~/.saves")))
(setq backup-directory-alist `((".#*" . "~/.saves")))
(setq backup-directory-alist `((".\#*" . "~/.saves")))
(setq backup-directory-alist `((".tern*" . "~/.saves")))


;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;
(require 'package) (package-initialize)
;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;

;;----menu-bar-open f10 doesnt work------------;
 (global-set-key (kbd "C-c C-7") (quote menu-bar-open))
 (global-set-key [3 38] (quote menu-bar-open))
;;----menu-bar-open f10 doesnt work------------;

;;----menu-bar-open f10 doesnt work------------;
 (global-set-key (kbd "C-c C-b") (quote buffer-menu))
;;(global-set-key [3 38] (quote buffer-menu))
;;----menu-bar-open f10 doesnt work------------;

;;----yas-activate-extra-mode------------;
 (global-set-key (kbd "C-c C-y") (quote yas-activate-extra-mode))

;;----auto-complete-mode------------;
 (global-set-key (kbd "C-x C-a") (quote auto-complete-mode))
;; (global-set-key (kbd "C-c C-a") (quote auto-complete-mode))
;;----ac-complete-tern-complete------------;
;; (global-set-key (kbd "C-c C-w") (quote ac-complete-tern-completion))

;;----js2-refactor-mode------------;
 (global-set-key (kbd "C-x C-g") (quote js2-refactor-mode))
;;----js2-refactor-mode------------;

;;----neotree------------------------------------------------------------------;;
;; (global-set-key [3 14] (quote neotree-toggle))
(global-set-key (kbd "C-c C-^") (quote neotree-toggle))
(add-to-list 'load-path "~/.emacs.d/elpa/neotree-20170522.758/")
(add-to-list 'load-path "~/.emacs.d/elpa/all-the-icons-20180125.757/all-the-icons.el")
(require 'all-the-icons-gnus)
(all-the-icons-gnus-setup)
;;To use this package, do

(require 'neotree)
;(setq doom-neotree-enable-file-icons t)
(require 'all-the-icons)
;;(mode-icons-mode)
;;(require 'all-the-icons-gnus)
;;(all-the-icons-gnus-setup)
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
(add-to-list 'load-path "~/.emacs.d/elpa/neotree-20170522.758")
(add-to-list 'load-path "~/.local/share/icons-in-terminal/")(add-to-list 'load-path "~/icons-in-terminal/")
;;(require 'icons-in-terminal)
;;(insert (icons-in-terminal 'oct_flame)) ; C-h f icons-in-terminal[RET] for more info
(require 'neotree)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)
(setq neo-theme 'classic)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(setq-default neo-smart-open t)
;;---endneotree----------------------------------------------------------------;;

;;-------handlebars mode--------------------------------------;;
   (require 'handlebars-mode)
;;-------handlebars mode--------------------------------------;;

;;------------line-mode and macro for comment and indent----------------------------------;;
(add-hook 'prog-mode-hook 'linum-mode)
(fset 'indent-buffer
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("< >xindent-region" 0 "%d")) arg)))
 ; global-set-key
 ; repeat-complex-command
(global-set-key [27 80] (quote indent-buffer))
;;(global-set-key [3 9] (quote indent-buffer)) ; ctl-c ctl-i
(global-set-key [3 3] (quote comment-region)); ctl-c ctl-c
(global-set-key [3 21] (quote uncomment-region)) ;; ctl-c ctl-u
(ido-mode 1)
(global-linum-mode)
(setq linum-format "%4d \u2502 ")
;; Show-hide
(global-set-key (kbd "") 'hs-show-block)
(global-set-key (kbd "") 'hs-show-all)
(global-set-key (kbd "") 'hs-hide-block)
(global-set-key (kbd "") 'hs-hide-all)
;;------------line-mode and macro for comment and indent end----------------------------------;;

;;TODO: fix kbd for window resize:
;; ------keybinds for enlarge windows----------------
;; (global-set-key (kbd "A-<down>") 'enlarge-window)
;; (global-set-key (kbd "A-<up>") 'shrink-window)
;; (global-set-key (kbd "A-<left>") 'enlarge-window-horizontally)
;; (global-set-key (kbd "A-<right>") 'shrink-window-horizontally)
;; ---end Enlarge windows keybindings -------------

(cond ((< emacs-major-version 25)
        (setq custom-file "~/.emacs.react.el"))
      (t (setq custom-file "~/.emacs.react.el")))

;;http://www.flycheck.org/manual/latest/index.html ;;No one likes to type a full yes, y is enough as a confirmation.
(defalias 'yes-or-no-p 'y-or-n-p)         ;;For easily identification of the current line.;;(global-hl-line-mode) ;;;this works--------------
(require 'smartparens-config)(require 'js) (add-hook 'js-mode-hook #'smartparens-mode)   (smartparens-global-mode)

;; Testing adding these from: http://ergoemacs.org/emacs/emacs_highlight_parenthesis.html
;; highlight brackets if visible, else entire expression
; (setq show-paren-style 'mixed)
; (setq show-paren-style 'expression)
(setq show-paren-style 'parenthesis)
(require 'highlight-parentheses)
;;&&&&&&&&&&&&&&&&&&&&&&&& end of my config&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

(provide '.emacs.keybinds-neotree)
;;; .emacs.keybinds-neotree.el ends here
