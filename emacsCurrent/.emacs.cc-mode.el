;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(package-initialize)

;; from https://truongtx.me/2013/03/10/emacs-setting-up-perfect-environment-for-cc-programming
(require 'cc-mode)

(setq-default c-basic-offset 4 c-default-style "linux")
(setq-default tab-width 4 indent-tabs-mode t)
(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)

;;(require 'autopair)
;;(autopair-global-mode 1)
;; (setq autopair-autowrap t)

;(require 'auto-complete-clang)
;(define-key c++-mode-map (kbd "C-S-<return>") 'ac-complete-clang)
;; replace C-S-<return> with a key binding that you want

;; -auto-complete-c-headers
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; https://emacs.stackexchange.com/questions/22556/auto-complete-c-headers-just-works-with-c-no-result-with-c;;
  ;; start auto-complete with emacs																		     ;;
   (require 'auto-complete)																				     ;;
  ;; do default config for auto-complete																     ;;
   (require 'auto-complete-config)																		     ;;
   (ac-config-default)																					     ;;
  ;; let's define a function which initializes auto-complete-c-headers and gets called for c/c++ hooks	     ;;
   (defun my:ac-c-header-init ()																		     ;;
   (require 'auto-complete-c-headers)																	     ;;
   (add-to-list 'ac-sources 'ac-source-c-headers)														     ;;
   (setq achead:include-directories																		     ;;
   	  (append '("/usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/x86_64-pc-linux-gnu			     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/backward						     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include														     ;;
    /usr/local/include																					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed												     ;;
    /usr/include ")																						     ;;
                achead:include-directories)))															     ;;
  ;; now let's call this funcion from c/c++ hooks														     ;;
   (add-hook 'c++-mode-hook 'my:ac-c-header-init)														     ;;
   (add-hook 'c-mode-hook 'my:ac-c-header-init)															     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; iedit mode baby								 ;;
   ;; fix bug										 ;;
   (define-key global-map (kbd "C-c ;") 'iedit-mode) ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; turn on semantic mode
(semantic-mode 1)
;; let's define a function which adds semantic as a suggestion backend to auto complete and hook  this function to c-mode-common-hook
;; BUT we don't need it we just turn on company-mode
(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic)
  )
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)
;; turn on ede mode
(global-ede-mode 1)
;; create project for our program
(ede-cpp-root-project "TEst"
					  :file "/home/arcarius/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp"
					  :include-path '("/../my_inc"))
;;you can use system-include-path for setting up the system header file locations
 (load "~/.emacs.semantic.el")


;;(require 'semantic)
;;(global-semanticdb-minor-mode 1)
;;(global-semantic-idle-scheduler-mode 1)
;;(semantic-mode 1)
;;(semantic-enable)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (defun company-semantic-setup ()                                                                                      ;;
   "Configure company-backends for company-semantic and company-yasnippet."                                            ;;
   (delete 'company-irony company-backends)                                                                            ;;
   (push '(company-semantic :with company-yasnippet) company-backends)                                                 ;;
   )                                                                                                                   ;;
                                                                                                                       ;;
 ;;--------------------------------------------------------------------                                                ;;
 (global-ede-mode 1)                                                                                                   ;;
 (setq ede-custom-file (expand-file-name "cc-mode-projects.el" user-emacs-directory))                                  ;;
 (when (file-exists-p ede-custom-file)                                                                                 ;;
   (load ede-custom-file))                                                                                             ;;
                                                                                                                       ;;
;;--------------------------------------------------------------------                                                ;;
(ede-cpp-root-project "TEst"
		      :file "/home/arcarius/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp"
		      :include-path '("/../my_inc/")
                      :system-include-path '("~/DevWork/UdemyCourse/C_for_beginners/demos/my_program/my_inc"                                                                ;;
 					     ))
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; (ede-cpp-root-project "my_program"                                                                                    ;;                                                             ;;
 ;; 		      :file "/home/arcarius/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp"           ;;                                                              ;;
 ;; 		      :include-path '("~/DevWork/UdemyCourse/C_for_beginners/demos/my_program/my_inc"                                                                      ;;            ;;
 ;; 				      "src")                                                                             ;;                                                              ;;
 ;; 		      :system-include-path '("~/DevWork/UdemyCourse/C_for_beginners/demos/my_program/my_inc"                                                                ;;           ;;
 ;; 					     ))                                                                          ;;                                                              ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                                                                                                                       ;;
 (defun company-c-headers-setup ()                                                                                     ;;
   (add-to-list 'company-backends 'company-c-headers))                                                                 ;;
                                                                                                                       ;;
 ;;--------------------------------------------------------------------                                                ;;
 (defun ede-object-system-include-path ()                                                                              ;;
   (when ede-object                                                                                                    ;;
     (ede-system-include-path ede-object)))                                                                            ;;
 (setq company-c-headers-path-system 'ede-object-system-include-path)                                                  ;;
                                                                                                                       ;;
 ;;--------------------------------------------------------------------                                                ;;
 (setq header-custom-file (expand-file-name "cc-mode-header-custom.el" user-emacs-directory))                          ;;
 (when (file-exists-p header-custom-file)                                                                              ;;
   (load header-custom-file))                                                                                          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;--------------------------------------------------------------------
(add-hook 'c++-mode-hook 'company-c-headers-setup)
(add-hook 'c-mode-hook 'company-c-headers-setup)
(add-hook 'c++-mode-hook 'company-semantic-setup)
(add-hook 'c-mode-hook 'company-semantic-setup)

;;--------------------------------------------------------------------





(provide '.emacs.cc-mode);;;
;;; .emacs.cc-mode ends here
