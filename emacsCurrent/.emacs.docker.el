;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;;     This box is for my Dockerfile syntax                        ;;
 ;;                                                                 ;;
 ;;  yaml for .yml files                                            ;;
      (require 'yaml-mode)                                          ;;
      (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))     ;;
 ;;Unlike python-mode, this mode follows the Emacs convention       ;;
 ;;of not binding the ENTER key to `newline-and-indent'.            ;;
 ;;To get this                                                      ;;
 ;;behavior, add the key definition to `yaml-mode-hook':            ;;
   (add-hook 'yaml-mode-hook                                        ;;
    '(lambda ()                                                     ;;
       (define-key yaml-mode-map "\C-m" 'newline-and-indent)))      ;;
                                                                    ;;
;;  this is for the dockerfile syntax                               ;;
;;(add-to-list 'load-path "/your/path/to/dockerfile-mode/")         ;;
 (require 'dockerfile-mode)                                         ;;
 (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode));;
;;                                                                  ;;
;; You can specify the image name in the file itself by adding      ;;
;; a line like this at the top of your Dockerfile.                  ;;
;;  ## -*- docker-image-name: "your-image-name-here" -*-            ;;
;;  If you don't, you'll be prompted for an image name each time you;;
;;  build.                                                          ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;; For integrating docker.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; `docker-tramp.el' offers a TRAMP method for Docker containers.										    ;;
;; 																											    ;;
;; ;; > **NOTE**: `docker-tramp.el' relies in the `docker exec` command.  Tested							    ;;
;; ;; > with docker version 1.6.x but should work with versions >1.3										    ;;
;; 																											    ;;
;; ;; ## Usage																								    ;;
;; 																											    ;;
;; ;; Offers the TRAMP method `docker` to access running containers											    ;;
;; 																											    ;;
;; ;;     C-x C-f /docker:user@container:/path/to/file														    ;;
;; 																											    ;;
;; ;;     where																								    ;;
;; ;;       user           is the user that you want to use (optional)										    ;;
;; ;;       container      is the id or name of the container												    ;;
;; 																											    ;;
;; ;; ## Troubleshooting																					    ;;
;; 																											    ;;
;; ;; ### Tramp hangs on Alpine container																	    ;;
;; 																											    ;;
;; ;; Busyboxes built with the `ENABLE_FEATURE_EDITING_ASK_TERMINAL' config option							    ;;
;; ;; send also escape sequences, which `tramp-wait-for-output' doesn't ignores								    ;;
;; ;; correctly.  Tramp upstream fixed in [98a5112][] and is available since								    ;;
;; ;; Tramp>=2.3.																							    ;;
;; 																											    ;;
;; ;; For older versions of Tramp you can dump [docker-tramp-compat.el][] in your							    ;;
;; ;; `load-path' somewhere and add the following to your `init.el', which									    ;;
;; ;; overwrites `tramp-wait-for-output' with the patch applied:											    ;;
;; 																											    ;;
;; ;;     (require 'docker-tramp-compat)																	    ;;
;; 																											    ;;
;; ;; [98a5112]: http://git.savannah.gnu.org/cgit/tramp.git/commit/?id=98a511248a9405848ed44de48a565b0b725af82c ;;
;; ;; [docker-tramp-compat.el]: https://github.com/emacs-pe/docker-tramp.el/raw/master/docker-tramp-compat.el   ;;
;; 																											    ;;
;; ;; [back]																								    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Code:
;; This is for `docker' package
(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

;; This is from dockerfile mode:
;; https://github.com/spotify/dockerfile-mode
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Code:                                                                            ;;
;; (add-to-list 'load-path "/your/path/to/dockerfile-mode/")		                 ;;
(add-to-list 'load-path "/home/arcarius/.emacs.d/elpa/dockerfile-mode-20181104.1800");;
(require 'dockerfile-mode)										                     ;;
(add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; **Moved this from .emacs test to see if needed**
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  DockerFile-mode                                                                     ;;
   (add-to-list 'load-path "/home/arcarius/.emacs.d/elpa/dockerfile-mode-20181104.1800");;
   (require 'dockerfile-mode)                                                           ;;
   (add-to-list 'auto-mode-alist '("Dockerfile\\'" . dockerfile-mode))                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(provide '.emacs.docker)
;;; .emacs.docker.el ends here
