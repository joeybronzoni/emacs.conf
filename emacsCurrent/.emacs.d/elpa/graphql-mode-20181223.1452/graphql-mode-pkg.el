;;; -*- no-byte-compile: t -*-
(define-package "graphql-mode" "20181223.1452" "Major mode for editing GraphQL schemas" '((emacs "24.3")) :commit "0f2b4b16049b7b4043575f0ec592305624a8775c" :keywords '("languages") :authors '(("David Vazquez Pua" . "davazp@gmail.com")) :maintainer '("David Vazquez Pua" . "davazp@gmail.com"))
