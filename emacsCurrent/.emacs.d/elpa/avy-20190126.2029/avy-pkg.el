;;; -*- no-byte-compile: t -*-
(define-package "avy" "20190126.2029" "Jump to arbitrary positions in visible text and select text quickly." '((emacs "24.1") (cl-lib "0.5")) :commit "91240220ad8f5168527343d5aeda5f9acf5224cb" :keywords '("point" "location") :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :url "https://github.com/abo-abo/avy")
