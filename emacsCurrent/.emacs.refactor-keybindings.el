
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(setq load-path (cons (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/")
		      load-path))
(setq load-path (cons (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/")
		      load-path))

(add-to-list 'load-path (expand-file-name "~/.emacs.d/elpa/all-the-icons-20180125.757/"))
;;move backup copies to a different dir
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-directory-alist `((".tern-port" . "~/.saves")))
(setq backup-directory-alist `((".#*" . "~/.saves")))
(setq backup-directory-alist `((". #*" . "~/.saves")))
(setq backup-directory-alist `((" #*" . "~/.saves")))
(setq backup-directory-alist `((".#*" . "~/.saves")))
(setq backup-directory-alist `((".\#*" . "~/.saves")))
(setq backup-directory-alist `((".tern*" . "~/.saves")))

 (setq tern-command '("tern" "--no-port-file" "--persistent"))
 (setq make-backup-files nil) ; stop creating ~ files
 (setq create-lockfiles nil);;really...fucking stop
;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;
(require 'package) (package-initialize)
;;---THIS WAS THE LINE THAT SEEMED TO WORK-------LINK::https://stackoverflow.com/questions/16766830/emacs-load-path-and-require-cannot-open-load-file----;;

;; Fix git in terminal for error: Warning terminal is not fully functional
(setenv "PAGER" "cat")

;;----menu-bar-open f10 does work too------------;
(global-set-key (kbd "C-c C-7") (quote menu-bar-open))
(global-set-key [3 38] (quote menu-bar-open))
;;----menu-bar-open f10 doesnt work------------;

;;----menu-bar-open f10 doesnt work------------;
(global-set-key (kbd "C-c C-b") (quote buffer-menu))
;;(global-set-key [3 38] (quote buffer-menu))
;;----menu-bar-open f10 doesnt work------------;

;;=========MULTIPLE-CURSORS=======================;
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
;;to disable the return button exiting mc/mark-all-like-this-m OR!-
;;use C-j to add the newline instead of it killing mc/mark-all*
;;(define-key "mc/keymap" (kbd "<return>") nil)
;;=========multiple-cursors=======================;
;;----yas-activate-extra-mode------------;
;;(global-set-key (kbd "C-x C-y") (quote yas-activate-extra-mode))

;;--------COMPANY-YASNIPPET-------------
(global-set-key [C-c C-y] 'company-mode)
(global-set-key (kbd "C-x C-y") (quote company-mode))
(global-set-key [C-c C-t] 'tern-mode)
(global-set-key (kbd "C-x C-t") (quote tern-mode))

;;----AMD-MODE------------;
(global-set-key (kbd "C-x C-m") (quote amd-mode))
;;----amd-mode------------;

;;----AUTO-COMPLETE-MODE------------;
(global-set-key (kbd "C-x C-a") (quote auto-complete-mode))
;; (global-set-key (kbd "C-c C-a") (quote auto-complete-mode))
;;----ac-complete-tern-complete------------;
;; (global-set-key (kbd "C-c C-w") (quote ac-complete-tern-completion))

;;----JS2-REFACTOR-MODE------------;
(global-set-key (kbd "C-x C-g") (quote js2-refactor-mode))
;;----js2-refactor-mode------------;


;;----HIGHLIGHT PARENTHESIS MODE------------;
; Note that I might need to hit '?' for whitespace-toggle to follow through
(global-set-key (kbd "C-c [") (quote whitespace-toggle-options))
(global-set-key (kbd "C-x p") (quote highlight-parentheses-mode))
;;----HIGHLIGHT PARENTHESIS MODE------------;



(require 'all-the-icons)
;;~~~~~~~~~~########## all-the-icons ############
(add-to-list 'load-path "~/.emacs.d/elpa/neotree-20170522.758/")
(add-to-list 'load-path "~/.emacs.d/elpa/all-the-icons-20180125.757/all-the-icons.el")
(require 'all-the-icons-gnus)
(all-the-icons-gnus-setup)
;;To use this package, do
(all-the-icons-ivy-setup)
;;Or if you prefer to only set transformers
;;for a subset of ivy commands:
(require 'all-the-icons-ivy)
(ivy-set-display-transformer 'ivy-switch-buffer 'all-the-icons-ivy-buffer-transformer)
;;~~~~~~~~~########## End all-the-icons ############



;;----NEOTREE------------------------------------------------------------------;;
;; (global-set-key [3 14] (quote neotree-toggle))
(global-set-key (kbd "C-c C-^") (quote neotree-toggle))
(require 'neotree)
;(setq doom-neotree-enable-file-icons t)
(require 'all-the-icons)
;;(mode-icons-mode)
;;(require 'all-the-icons-gnus)
;;(all-the-icons-gnus-setup)
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
(add-to-list 'load-path "~/.emacs.d/elpa/neotree-20170522.758")
(add-to-list 'load-path "~/.local/share/icons-in-terminal/")(add-to-list 'load-path "~/icons-in-terminal/")
;;(require 'icons-in-terminal)
;;(insert (icons-in-terminal 'oct_flame)) ; C-h f icons-in-terminal[RET] for more info
(require 'neotree)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)
(setq neo-theme 'classic)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(setq-default neo-smart-open t)
;;---endneotree----------------------------------------------------------------;;


;;-------HANDLEBARS MODE--------------------------------------;;
(require 'handlebars-mode)
;;-------handlebars mode--------------------------------------;;

;;-------TYPESCRIPT MODE tide-mode--------------------------------------;;
(global-set-key (kbd "C-x c") (quote tide-compile-file))
(global-set-key (kbd "C-x t") (quote tide-restart-server))

;;-------Typescript mode--------------------------------------;;


;;-------BROWSER--------------------------------------;;
(global-set-key (kbd "C-c C-\\") (quote browse-url))
;;-------BROWSER--------------------------------------;;


;;------------line-mode and macro for comment and indent----------------------------------;;
(add-hook 'prog-mode-hook 'linum-mode)
(fset 'indent-buffer
      (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ("< >xindent-region" 0 "%d")) arg)))
					; global-set-key
					; repeat-complex-command
(global-set-key [27 80] (quote indent-buffer))
(global-set-key [3 9] (quote indent-buffer)) ; ctl-c ctl-i
(global-set-key [3 3] (quote comment-region)); ctl-c ctl-c
(global-set-key [3 21] (quote uncomment-region)) ;; ctl-c ctl-u
(ido-mode 1)
(global-linum-mode)
(setq linum-format "%4d \u2502 ")
;; Show-hide
(global-set-key (kbd "") 'hs-show-block)
(global-set-key (kbd "") 'hs-show-all)
(global-set-key (kbd "") 'hs-hide-block)
(global-set-key (kbd "") 'hs-hide-all)
;;------------line-mode and macro for comment and indent end----------------------------------;;

(cond ((< emacs-major-version 24)
       (setq custom-file "~/.emacs.react.el"))
      (t (setq custom-file "~/.emacs.react.el")))
(defalias 'yes-or-no-p 'y-or-n-p)
;;For easily identification of the current line.
;;(global-hl-line-mode)
;;;this works------------------------------------------------
;;http://www.flycheck.org/manual/latest/index.html
;;No one likes to type a full yes, y is enough as a confirmation.
(require 'smartparens-config)
(require 'js)
(add-hook 'js-mode-hook #'smartparens-mode)
(smartparens-global-mode)

;;-------------------------------------------------------------------------------------
(provide '.emacs.refactor-keybindings.el)
;;; .emacs.refactor-keybindings.el ends here
