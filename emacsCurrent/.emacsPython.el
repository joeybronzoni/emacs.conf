;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for python, jupyter-notebook, with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(package-initialize)


(defvar jedi:setup
  '(better-defaults
    elpy ;; add the elpy package
    material-theme))

(elpy-enable)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "~/.emacs.company-yasnippet.el") ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "~/.emacs.jedi_starter.el") ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    ;;^^^^^^^for keybindings, icons, neotree, etc^^^^^^^^ ;;
;;    (load "~/.emacs.refactor-keybindings.el")             ;;
;; ;;^^^^^^^ End for keybindings, icons, neotree, etc^^^^^^^;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;=======for the awesome config from=====> https://github.com/foretagsplatsen/emacs-js      ;;
;;    (load "~/.emacs.refactor-flycheck.el")                                                      ;;
;;    (load "~/.emacs.refactor.el")                                                               ;;
;; ;; ;;======= END for the awesome config from=====> https://github.com/foretagsplatsen/emacs-js ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; ;;------for the awesome config from----> https://github.com/phardyn/emacs ;;
;;    (load "~/.emacs.fortags.el")                                              ;;
;; ;; ;;------ END for the awesome config from https://github.com/phardyn/emacs ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;------for the awesome config from----> https://github.com/phardyn/emacs ;;
   (load "~/.emacs.xwidget_youtube.el")                                              ;;
;; ;;------ END for the awesome config from https://github.com/phardyn/emacs ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(provide '.emacs)
;;; .emacsPython ends here
