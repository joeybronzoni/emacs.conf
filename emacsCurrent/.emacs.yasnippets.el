

(setq ac-modes '(js2-mode flow-jsx-mode jsx-mode js3-mode))






;;==== yasnippet==================================================;;
;;+_+_+_+_+_+_+_+_+_+__+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
   (add-to-list 'interpreter-mode-alist '("node" . js2-mode))
   (add-to-list 'auto-mode-alist '("\\.jsx?\\'" . js2-jsx-mode))
   (add-to-list 'auto-mode-alist '("\\.js?\\'" . js2-jsx-mode))
   (add-to-list 'interpreter-mode-alist '("node" . js2-jsx-mode))
   (add-hook 'javascript-mode-hook (lambda () (yas/minor-mode-on)))
 ;;+_+_+_+_+_+_+_+_+_+__+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
 ;; yasnippet
 ;;; should be loaded before auto complete so that they can work together



 (yas-global-mode t)
 ;;(yas-global-mode t)
 ;;; auto complete mod
 ;;; should be loaded after yasnippet so that they can work together
 (global-auto-complete-mode t)
;;=-=-
(add-to-list 'load-path "~/.emacs.d/es6-snippets")
   (require 'es6-snippets)
 ;;-=-=-
 ;;(add-to-list 'load-path "~/.emacs.d/react-templates")
 ;;(require 'react-templates)
 (setq ac-source-yasnippet nil)
 (set-default 'ac-sources
              '(ac-source-abbrev
                ac-source-dictionary
                ac-source-yasnippet
                ac-source-words-in-buffer
                ac-source-words-in-same-mode-buffers
                ac-source-semantic))
 (setq yas-snippet-dirs
       '("~/.emacs.d/snippets"                 ;; personal snippets
         "/.emacs.d/snippets/js-mode"           ;; foo-mode and bar-mode snippet collection
         "/path/to/yasnippet/yasmate/snippets" ;; the yasmate collection
         "/path/to/yasnippet/snippets"         ;; the default collection
         ))
 (ac-config-default)
 (setq ac-use-menu-map t)
 (eval-after-load "auto-complete"
      '(add-to-list 'ac-sources 'ac-source-yasnippet))
 ;;; set the trigger key so that it can work together with yasnippet on tab key,
 ;;; if the word exists in yasnippet, pressing tab will cause yasnippet to
 ;;; activate, otherwise, auto-complete will
 (ac-set-trigger-key "TAB")
 (ac-set-trigger-key "<tab>")
 ;; trigger using TAB and disable auto-start
 (custom-set-variables
  '(ac-trigger-key "TAB")
  '(ac-auto-start nil)
  '(ac-use-menu-map t))
 (defun ac-js-mode()
   (setq ac-sources '(ac-source-yasnippet
                      ac-source-symbols
                      ac-source-words-in-buffer
                      ac-source-words-in-same-mode-buffers
                      ac-source-files-in-current-dir
                      )))
(add-hook 'js-mode-hook 'ac-js-mode) (add-hook 'jsx-mode-hook 'ac-js-mode) (add-hook 'flow-jsx-mode-hook 'ac-js-mode)
 (add-hook 'rjsx-mode-hook 'ac-js-mode)
;; (add-hook 'js3-mode 'flow-jsx-mode 'js-mode-hook 'ac-js-mode)
 ;;---- yasnippet-------------------------------------------------;;



