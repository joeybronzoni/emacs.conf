;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(semantic-mode 1)

(require 'semantic/ia)

(require 'semantic/bovine/gcc)

(semantic-add-system-include "~/DevWork/UdemyCourse/C_for_beginners/demos/my_program/my_inc" 'c++-mode)

(setq-mode-local c-mode semanticdb-find-default-throttle
                 '(project unloaded system recursive))


(defun my:ac-c-header-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers)
  (add-to-list 'achead:include-directories '"/usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/x86_64-pc-linux-gnu			     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/backward						     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include														     ;;
    /usr/local/include																					     ;;
    /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed												     ;;
    /usr/include")
  )

(add-hook 'c++-mode-hook 'my:ac-c-header-init)
(add-hook 'c-mode-hook 'my:ac-c-header-init)

(define-key global-map (kbd "C-c ;") 'iedit-mode)

(semantic-mode 1)

(defun my:add-semantic-to-autocomplete()
  (add-to-list 'ac-sources 'ac-source-semantic)
  )
(add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)

(global-ede-mode 1)
;; create project for our program
(ede-cpp-root-project "TEst"
		      :file "/home/arcarius/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp"
		      :include-path '("/../my_inc/"))
;;you can use system-include-path for setting up the system header file locations

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;    (require 'auto-complete-config)																		     ;;                           ;;
;;    (ac-config-default)																					     ;;           ;;
;;   ;; let's define a function which initializes auto-complete-c-headers and gets called for c/c++ hooks	     ;;                                                                                           ;;
;;    (defun my:ac-c-header-init ()																		     ;;                           ;;
;;    (require 'auto-complete-c-headers)																	     ;;                           ;;
;;    (add-to-list 'ac-sources 'ac-source-c-headers)														     ;;                                           ;;
;;    (setq achead:include-directories																		     ;;                           ;;
;;    	  (append '("/usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1					     ;;                                                                           ;;
;;     /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/x86_64-pc-linux-gnu			     ;;                                                                                           ;;
;;     /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/backward						     ;;                                                                           ;;
;;     /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include														     ;;                                           ;;
;;     /usr/local/include																					     ;;           ;;
;;     /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed												     ;;                                                   ;;
;;     /usr/include ")																						     ;;           ;;
;;                 achead:include-directories)))															     ;;                                   ;;
;;   ;; now let's call this funcion from c/c++ hooks														     ;;                                           ;;
;;    (add-hook 'c++-mode-hook 'my:ac-c-header-init)														     ;;                                           ;;
;;    (add-hook 'c-mode-hook 'my:ac-c-header-init)															     ;;                                   ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                                                                                                                                                ;;
;; ;; ;; if you want to enable support for gnu global           ;;                                                                                                                                                ;;
;; ;; (when (cedet-gnu-global-version-check t)                  ;;                                                                                                                                                ;;
;; ;;   (semanticdb-enable-gnu-global-databases 'c-mode)        ;;                                                                                                                                                ;;
;; ;;   (semanticdb-enable-gnu-global-databases 'c++-mode))     ;;                                                                                                                                                ;;
;; ;;                                                           ;;                                                                                                                                                ;;
;; ;; ;; enable ctags for some languages:                       ;;                                                                                                                                                ;;
;; ;; ;;  Unix Shell, Perl, Pascal, Tcl, Fortran, Asm           ;;                                                                                                                                                ;;
;; ;; (when (cedet-ectag-version-check t)                       ;;                                                                                                                                                ;;
;; ;;   (semantic-load-enable-primary-exuberent-ctags-support)) ;;                                                                                                                                                ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;                                                                                                                                                ;;
;; (global-ede-mode 1)                                                                                                                                                                                            ;;
;;                                                                                                                                                                                                                ;;
;;                                                                                                                                                                                                                ;;
;; (ede-cpp-root-project "Test"                                                                ;;                                                                                                                 ;;
;;                 :name "Test Project"                                                        ;;                                                                                                                 ;;
;;                 :file "~/DevWork/UdemyCourse/C_for_beginners/demos/my_program/src/main.cpp" ;;                                                                                                                 ;;
;;                 :include-path '("/../my_inc/"                                                ;;                                                                                                                ;;
;;                                ))                                                            ;;                                                                                                                ;;
;; (global-semantic-idle-scheduler-mode 1)                                                                                                                                                                        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide '.emacs.Fuck.el);;;
;;; .emacs.Fuck ends here
