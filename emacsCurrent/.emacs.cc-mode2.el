;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

(package-initialize)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; from https://truongtx.me/2013/03/10/emacs-setting-up-perfect-environment-for-cc-programming ;;
   (require 'cc-mode)                                                                             ;;
                                                                                                  ;;
   (setq-default c-basic-offset 4 c-default-style "linux")                                        ;;
   (setq-default tab-width 4 indent-tabs-mode t)                                                  ;;
   (define-key c-mode-base-map (kbd "RET") 'newline-and-indent)                                   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; -auto-complete-c-headers                                                                                                                                                                                                 ;;
   ;; https://emacs.stackexchange.com/questions/22556/auto-complete-c-headers-just-works-with-c-no-result-with-c;;                                                                                                ;;           ;;
      ;; start auto-complete with emacs																		     ;;                           ;;           ;;
       (require 'auto-complete)																				     ;;                   ;;           ;;
      ;; do default config for auto-complete																     ;;                                   ;;           ;;
       (require 'auto-complete-config)																		     ;;                           ;;           ;;
       (ac-config-default)																					     ;;           ;;           ;;
      ;; let's define a function which initializes auto-complete-c-headers and gets called for c/c++ hooks	     ;;                                                                                           ;;           ;;
       (defun my:ac-c-header-init ()																		     ;;                           ;;           ;;
       (require 'auto-complete-c-headers)																	     ;;                           ;;           ;;
       (add-to-list 'ac-sources 'ac-source-c-headers)														     ;;
	   ;; to get list of headers in terminal:
	   ;; gcc -xc++ -E -v -
	   ;; `gcc -print-prog-name=cc1` -v
	   ;; `gcc -print-prog-name=cc1plus` -v
       (setq achead:include-directories
       		 (append '("/usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/x86_64-pc-linux-gnu
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/../../../../include/c++/8.1.1/backward
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include
        /usr/local/include
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed
        /usr/include
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include
        /usr/local/include
        /usr/lib/gcc/x86_64-pc-linux-gnu/8.1.1/include-fixed
        /usr/include

         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1
         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/x86_64-pc-linux-gnu
         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/backward
         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include
         /usr/local/include
         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include-fixed
         /usr/include

         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include
         /usr/local/include
         /usr/lib/gcc/x86_64-pc-linux-gnu/8.2.1/include-fixed
         /usr/include

          /usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1
          /usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/x86_64-pc-linux-gnu
          /usr/bin/../lib64/gcc/x86_64-pc-linux-gnu/8.2.1/../../../../include/c++/8.2.1/backward
          /usr/local/include
          /usr/lib/clang/8.0.0/include
          /usr/include
          /home/arcarius/DevWork/UdemyCourse/Modern_C++/demos/my_program/my_inc
         ")																						     ;;           ;;                           ;;
                    achead:include-directories)))															     ;;                                   ;;           ;;
      ;; now let's call this funcion from c/c++ hooks														     ;;                                           ;;           ;;
       (add-hook 'c++-mode-hook 'my:ac-c-header-init)														     ;;                                           ;;           ;;
       (add-hook 'c-mode-hook 'my:ac-c-header-init)															     ;;                                   ;;           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
      ;; iedit mode baby                                 ;;
      ;; fix bug                                         ;;
      (define-key global-map (kbd "C-c ;") 'iedit-mode)  ;;
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;From: https://www.youtube.com/watch?v=Ib914gNr0ys                                                                                          ;;
   ;; turn on semantic mode                                                                                                                     ;;
   (semantic-mode 1)                                                                                                                            ;;
   ;; let's define a function which adds semantic as a suggestion backend to auto complete and hook  this function to c-mode-common-hook        ;;
   ;; BUT we don't need it we just turn on company-mode                                                                                         ;;
   (defun my:add-semantic-to-autocomplete()                                                                                                     ;;
     (add-to-list 'ac-sources 'ac-source-semantic)                                                                                              ;;
     )                                                                                                                                          ;;
   (add-hook 'c-mode-common-hook 'my:add-semantic-to-autocomplete)                                                                              ;;
   ;; turn on ede mode                                                                                                                          ;;
   (global-ede-mode 1)                                                                                                                          ;;
   ;; create project for our program                                                                                                            ;;
                                                                                                                                                ;;
   (ede-cpp-root-project "TEst"                                                                                                                 ;;
   		      :file "/home/arcarius/DevWork/UdemyCourse/Modern_C++/demos/my_program/src/main.cpp"                                  ;;
   		      :include-path '("/../my_inc/"))                                                                                           ;;
   ;;you can use system-include-path for setting up the system header file locations                                                            ;;
   ;;(my-semantic-parse-dir "~/demos/my_program/my_inc"  ".*regex")                                                                             ;;
    (load "~/.emacs.semantic.el")                                                                                                               ;;
                                                                                                                                                ;;
                                                                                                                                                ;;
   (require 'semantic)                                                                                                                          ;;
   (global-semanticdb-minor-mode 1)                                                                                                             ;;
   (global-semantic-idle-scheduler-mode 1)                                                                                                      ;;
   (semantic-mode 1)                                                                                                                            ;;
 ;;(semantic-enable)                                                                                                                            ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    (defun company-semantic-setup ()                                                                                      ;; ;;
      "Configure company-backends for company-semantic and company-yasnippet."                                            ;; ;;
      (delete 'company-irony company-backends)                                                                            ;; ;;
      (push '(company-semantic :with company-yasnippet) company-backends)                                                 ;; ;;
      )                                                                                                                   ;; ;;
                                                                                                                             ;;
    ;;--------------------------------------------------------------------                                                ;; ;;
    (global-ede-mode 1)                                                                                                   ;; ;;
    (setq ede-custom-file (expand-file-name "cc-mode-projects.el" user-emacs-directory))                                  ;; ;;
    (when (file-exists-p ede-custom-file)                                                                                 ;; ;;
      (load ede-custom-file))                                                                                             ;; ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;--------------------------------------------------------------------                                                      ;;
    (defun company-c-headers-setup ()                                                                                     ;;   ;;
      (add-to-list 'company-backends 'company-c-headers))                                                                 ;;   ;;

     ;;For IRONY-MODE with company (run M-x irony-install-server)
     (eval-after-load 'company
      '(add-to-list 'company-backends 'company-irony))
;;   ;;
    ;;--------------------------------------------------------------------                                                ;;   ;;
    (defun ede-object-system-include-path ()                                                                              ;;   ;;
      (when ede-object                                                                                                    ;;   ;;
        (ede-system-include-path ede-object)))                                                                            ;;   ;;
    (setq company-c-headers-path-system 'ede-object-system-include-path)                                                  ;;   ;;
                                                                                                                          ;;   ;;
    ;;--------------------------------------------------------------------                                                ;;   ;;
    (setq header-custom-file (expand-file-name "cc-mode-header-custom.el" user-emacs-directory))                          ;;   ;;
    (when (file-exists-p header-custom-file)                                                                              ;;   ;;
      (load header-custom-file))                                                                                          ;;   ;;
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
                                                                                                                               ;;
   ;;--------------------------------------------------------------------                                                      ;;
   (add-hook 'c++-mode-hook 'company-c-headers-setup)                                                                          ;;
   (add-hook 'c-mode-hook 'company-c-headers-setup)                                                                            ;;
   (add-hook 'c++-mode-hook 'company-semantic-setup)                                                                           ;;
   (add-hook 'c-mode-hook 'company-semantic-setup)                                                                             ;;
   ;;--------------------------------------------------------------------                                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 (defvar c-files-regex ".*\\.\\(c\\|cpp\\|h\\|hpp\\)"                                ;;
   "A regular expression to match any c/c++ related files under a directory")        ;;
                                                                                     ;;
 (defun my-semantic-parse-dir (root regex)                                           ;;
   "                                                                                 ;;
    This function is an attempt of mine to force semantic to                         ;;
    parse all source files under a root directory. Arguments:                        ;;
    -- root: The full path to the root directory                                     ;;
    -- regex: A regular expression against which to match all files in the directory ;;
   "                                                                                 ;;
   (let (                                                                            ;;
         ;;make sure that root has a trailing slash and is a dir                     ;;
         (root (file-name-as-directory root))                                        ;;
         (files (directory-files root t ))                                           ;;
        )                                                                            ;;
     ;; remove current dir and parent dir from list                                  ;;
     (setq files (delete (format "%s." root) files))                                 ;;
     (setq files (delete (format "%s.." root) files))                                ;;
     (while files                                                                    ;;
       (setq file (pop files))                                                       ;;
       (if (not(file-accessible-directory-p file))                                   ;;
           ;;if it's a file that matches the regex we seek                           ;;
           (progn (when (string-match-p regex file)                                  ;;
                (save-excursion                                                      ;;
                  (semanticdb-file-table-object file))                               ;;
            ))                                                                       ;;
           ;;else if it's a directory                                                ;;
           (my-semantic-parse-dir file regex)                                        ;;
       )                                                                             ;;
      )                                                                              ;;
   )                                                                                 ;;
 )                                                                                   ;;
                                                                                     ;;
 (defun my-semantic-parse-current-dir (regex)                                        ;;
   "                                                                                 ;;
    Parses all files under the current directory matching regex                      ;;
   "                                                                                 ;;
   (my-semantic-parse-dir (file-name-directory(buffer-file-name)) regex)             ;;
 )                                                                                   ;;
                                                                                     ;;
 (defun lk-parse-curdir-c ()                                                         ;;
   "                                                                                 ;;
    Parses all the c/c++ related files under the current directory                   ;;
    and inputs their data into semantic                                              ;;
   "                                                                                 ;;
   (interactive)                                                                     ;;
   (my-semantic-parse-current-dir c-files-regex)                                     ;;
 )                                                                                   ;;
                                                                                     ;;
 (defun lk-parse-dir-c (dir)                                                         ;;
   "Prompts the user for a directory and parses all c/c++ related files              ;;
    under the directory                                                              ;;
   "                                                                                 ;;
   (interactive (list (read-directory-name "Provide the directory to search in:")))  ;;
   (my-semantic-parse-dir (expand-file-name dir) c-files-regex)                      ;;
 )                                                                                   ;;
                                                                                     ;;
 (provide 'lk-file-search)                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(add-to-list 'load-path (expand-file-name "~/.emacs.d/irony/"))
(require 'irony)

(defun my:irony-enable()
  (when (member major-mode irony-mode)
	(irony-mode 1)))
(add-hook 'c++-mode-hook 'my:irony-enable)
(add-hook 'c-mode-hook 'my:irony-enable)
(push 'rjsx-mode irony-supported-major-modes)
(push 'js2-jsx-mode irony-supported-major-modes)

;; Projectile:
(setq projectile-project-search-path '("~/DevWork/UdemyCourse/C_for_beginners/" "~/DevWork/UdemyCourse/Modern_C++/"))


(provide '.emacs.cc-mode);;;
;;; .emacs.cc-mode ends here
