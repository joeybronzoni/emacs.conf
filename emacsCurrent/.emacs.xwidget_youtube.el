;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)

;; Browser settings:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;; set specific browser to open links                       ;;
(setq browse-url-browser-function 'browse-url-default-browser) ;;
;; (global-set-key (kbd "C-x C-c") (quote ))                   ;;
;(setq browse-url-browser-function 'browse-url-/usr/bin/brave) ;;
                                                               ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 (require 'xwidget)

(defun eww-tag-iframe (dom)
  (when (fboundp 'make-xwidget)
	"""Docstring here yo"""
    (require 'xwidget)
    (let ((xw (xwidget-insert 1 'webkit-osr  (current-buffer)
                              (string-to-number (dom-attr dom 'width))
                              (string-to-number (dom-attr dom 'height)))))
      (xwidget-webkit-goto-uri xw (dom-attr dom 'src)))))

;; make these keys behave like normal browser
(define-key xwidget-webkit-mode-map [mouse-4] 'xwidget-webkit-scroll-down)
(define-key xwidget-webkit-mode-map [mouse-5] 'xwidget-webkit-scroll-up)
(define-key xwidget-webkit-mode-map (kbd "<up>") 'xwidget-webkit-scroll-down)
(define-key xwidget-webkit-mode-map (kbd "<down>") 'xwidget-webkit-scroll-up)
(define-key xwidget-webkit-mode-map (kbd "M-w") 'xwidget-webkit-copy-selection-as-kill)
(define-key xwidget-webkit-mode-map (kbd "C-c") 'xwidget-webkit-copy-selection-as-kill)

;; adapt webkit according to window configuration chagne automatically
;; without this hook, every time you change your window configuration,
;; you must press 'a' to adapt webkit content to new window size
(add-hook 'window-configuration-change-hook (lambda ()
               (when (equal major-mode 'xwidget-webkit-mode)
                 (xwidget-webkit-adjust-size-dispatch))))

;; by default, xwidget reuses previous xwidget window,
;; thus overriding your current website, unless a prefix argument
;; is supplied
;;
;; This function always opens a new website in a new window
(defun xwidget-browse-url-no-reuse (url &optional sessoin)
  (interactive (progn
                 (require 'browse-url)
                 (browse-url-interactive-arg "xwidget-webkit URL: "
                                             )))
  (xwidget-webkit-browse-url url t))

;; make xwidget default browser
(setq browse-url-browser-function (lambda (url session)
                    (other-window 1)
                    (xwidget-browse-url-no-reuse url)))
