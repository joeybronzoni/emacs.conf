;;; package --- Summary


;;; Commentary:
;;; The following Config is setup for javascript and react with git-tree, yasnippets, xwidgets and more:

;;; Code:

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;;(add-to-list 'Info-default-directory-list "/usr/local/share/emacs/27.0.50/lisp")
;; (setq package-enable-at-startup nil)


;; If python breaks move this code block into .emacs after setenv block for node
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; TODO: Clean up python settings and put into one file																	    ;;
;; ;; ----------------------Testing this is orig-----------------															    ;;
;; ;; (setq py-python-command "python3")																					    ;;
;; ;; ;; *** This is for using the Udemy Nodejs Complete Course ***;;														    ;;
;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;;     (setenv "y" "yvalue")                                                                                              ;; ;;
;; ;;    (add-to-list 'exec-path "/usr/bin/python3.6")                               ;;           ;;						    ;;
;; ;;    (setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "/usr/bin/python3.6")))  ;; ;;						    ;;
;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; (defcustom python-shell-interpreter "python3.6"																		    ;;
;; ;;   "Default Python interpreter for shell."																				    ;;
;; ;;   :type 'string																										    ;;
;; ;;   :group 'python)																										    ;;
;; 																															    ;;
;; 																															    ;;
 (setq py-python-command "python")																						    ;;
 ;; *** This is for using the Udemy Nodejs Complete Course ***;;															    ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    ;;
     (setenv "y" "yvalue")                                                                                              ;;    ;;
    (add-to-list 'exec-path "/home/arcarius/anaconda3/bin/python")                               ;;           ;;			    ;;
    (setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "/home/arcarius/anaconda3/bin/python")))  ;; ;;			    ;;
 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    ;;
 (defcustom python-shell-interpreter "python"																				    ;;
   "Default Python interpreter for shell."																				    ;;
   :type 'string																											    ;;
   :group 'python)																										    ;;
;; 																															    ;;
;; 																															    ;;
;; 																															    ;;
;; ;; (setq py-python-command "python3")																					    ;;
;; ;; ;; *** This is for using the Udemy Nodejs Complete Course ***;;														    ;;
;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;;     (setenv "y" "yvalue")                                                                                              ;; ;;
;; ;;    (add-to-list 'exec-path "home/arcarius/anaconda2/bin/")                               ;;           ;;				    ;;
;; ;;    (setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "home/arcarius/anaconda2/bin/")))  ;; ;;				    ;;
;; ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ;;
;; ;; (defcustom python-shell-interpreter "python3.6"																		    ;;
;; ;;   "Default Python interpreter for shell."																				    ;;
;; ;;   :type 'string																										    ;;
;; ;;   :group 'python)																										    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(package-initialize)

;; INSTALL PACKAGES
;; --------------------------------------

;; Added this trying to get tables and charts to work
(add-hook 'LaTeX-mode-hook #'latex-extra-mode)
(require 'package)

;;(add-hook 'python-mode-hook 'jedi:setup)
;;(setq jedi:complete-on-dot t) ;;<----replaced these two lines with the following block of code

;; ***!!!***NOTE***!!!*** - I added this from the https://github.com/tkf/emacs-jedi/issues/280 -jaseemabid commented on Feb 21, 2017
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t
      jedi:use-shortcuts t
      jedi:environment-root "jedi"
      python-environment-directory "~/.virtualenvs")

(elpy-enable)
(setq elpy-rpc-backend "jedi")

;; ****!!!!!------>>>>>> added this to use python3.7 change to what I need:
;; But not I changed it because of conda envs -comment them out

;; (setq py-python-command "/usr/bin/python3.7")
;; (setq python-shell-interpreter "/usr/bin/python3.7")

(setq py-python-command "/home/arcarius/anaconda3/bin/python")
(setq python-shell-interpreter "/home/arcarius/anaconda3/bin/python")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (add-to-list 'package-archives								    ;;
;;        '("melpa" . "http://melpa.org/packages/") t)						    ;;
;; 												    ;;
;; (package-initialize)										    ;;
;; (when (not package-archive-contents)								    ;;
;;   (package-refresh-contents))								    ;;
;; 												    ;;
;; ;; Load & Install the 3 packages								    ;;
;; (defvar pythonPackageList									    ;;
;;   '(better-defaults										    ;;
;;     ein ;; add the ein package (emacs ipython)						    ;;
;;     material-theme										    ;;
;;     elpy ;; add the elpy package								    ;;
;;     flycheck ;; add the flycheck package							    ;;
;;     py-autopep8 ;; add the autopep8								    ;;
;;     ))											    ;;
;; 												    ;;
;; (mapc #'(lambda (package)									    ;;
;;     (unless (package-installed-p package)							    ;;
;;       (package-install package)))								    ;;
;;       pythonPackageList)									    ;;
;; 												    ;;
;; ;; BASIC CUSTOMIZATION									    ;;
;; ;; --------------------------------------							    ;;
;; 												    ;;
;; (setq inhibit-startup-message t) ;; hide the startup message					    ;;
;; (load-theme 'material t) ;; load material theme						    ;;
;; (global-linum-mode t) ;; enable line numbers globally					    ;;
;; 												    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	    ;;
;; ;; ;;-----When you type C-c C-c to run your python shell                               ;;	    ;;
;; ;;        you might get an error about readline support, there are                     ;;	    ;;
;; ;;        two patches but the 2nd should work- here is the                             ;;	    ;;
;; ;;        1st                                                                          ;;	    ;;
;; ;;                                                                                     ;;	    ;;
;; ;; (with-eval-after-load 'python                                                       ;;	    ;;
;; ;;   (defun python-shell-completion-native-try ()                                      ;;	    ;;
;; ;;     "Return non-nil if can trigger native completion."                              ;;	    ;;
;; ;;     (let ((python-shell-completion-native-enable t)                                 ;;	    ;;
;; ;;           (python-shell-completion-native-output-timeout                            ;;	    ;;
;; ;;            python-shell-completion-native-try-output-timeout))                      ;;	    ;;
;; ;;       (python-shell-completion-native-get-completions                               ;;	    ;;
;; ;;        (get-buffer-process (current-buffer))                                        ;;	    ;;
;; ;;        nil "_"))))                                                                  ;;	    ;;
;; ;; ;;----------------------------------------------------                              ;;	    ;;
;; ;; ;;or this                                                                           ;;	    ;;
;;  (setq python-shell-completion-native-enable nil)                                      ;;	    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	    ;;
;; 												    ;;
;; ;; Now enable elpy										    ;;
;; (elpy-enable)										    ;;
;; 												    ;;
;; 												    ;;
;; ;; get Flycheck on the horn for syntax							    ;;
;; (when (require 'flycheck nil t)								    ;;
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))				    ;;
;;   (add-hook 'elpy-mode-hook 'flycheck-mode))							    ;;
;; 												    ;;
;; (require 'py-autopep8)									    ;;
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)					    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;				    ;;
;; ;; (setq python-shell-interpreter "jupyter"                       ;;				    ;;
;; ;;       python-shell-interpreter-args "console --simple-prompt") ;;				    ;;
;; ;; (setq ein:completion-backend 'ein:use-ac-jedi-backend)         ;;				    ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;				    ;;
;; 												    ;;
;; 												    ;;
;; ;;(elpy-use-ipython)										    ;;
;; (setq python-shell-interpreter "ipython"							    ;;
;;       python-shell-interpreter-args "-i --simple-prompt")					    ;;
;; 												    ;;
;; 												    ;;
;;   (defun load-directory (dir)								    ;;
;;       (let ((load-it (lambda (f)								    ;;
;; 		       (load-file (concat (file-name-as-directory dir) f)))			    ;;
;; 		     ))										    ;;
;; 	(mapc load-it (directory-files dir nil "\\.el$"))))					    ;;
;; (load-directory "~/emacs_orig")								    ;;
;; (setq ein:completion-backend 'ein:use-ac-jedi-backend)					    ;;
;; (provide '.emacs)										    ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; .emacs.python.el ends here
