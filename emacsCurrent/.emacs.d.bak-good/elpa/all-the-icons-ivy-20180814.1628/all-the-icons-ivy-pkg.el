;;; -*- no-byte-compile: t -*-
(define-package "all-the-icons-ivy" "20180814.1628" "Shows icons while using ivy and counsel" '((emacs "24.4") (all-the-icons "2.4.0") (ivy "0.8.0")) :commit "789e3355274abf4bbb318d7574ccd4b9df6c4ad7" :keywords '("faces") :authors '(("asok")) :maintainer '("asok"))
