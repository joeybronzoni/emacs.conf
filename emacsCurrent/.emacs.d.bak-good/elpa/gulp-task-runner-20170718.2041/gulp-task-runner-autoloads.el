;;; gulp-task-runner-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "gulp-task-runner" "gulp-task-runner.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from gulp-task-runner.el

(autoload 'gulp "gulp-task-runner" "\
Prompt for a gulp task and run it.
With PREFIX or when called interactively with a prefix argument,
forces reload of the task list from gulpfile.js.

\(fn &optional PREFIX)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gulp-task-runner" '("gulp--")))

;;;***

;;;### (autoloads nil nil ("gulp-task-runner-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; gulp-task-runner-autoloads.el ends here
