;;; widgetjs-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "widgetjs" "widgetjs.el" (0 0 0 0))
;;; Generated autoloads from widgetjs.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "widgetjs" '("widgetjs-")))

;;;***

;;;### (autoloads nil nil ("widgetjs-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; widgetjs-autoloads.el ends here
