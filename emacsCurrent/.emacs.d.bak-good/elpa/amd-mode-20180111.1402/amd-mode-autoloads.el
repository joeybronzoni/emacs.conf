;;; amd-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "amd-mode" "amd-mode.el" (0 0 0 0))
;;; Generated autoloads from amd-mode.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "amd-mode" '("amd-")))

;;;***

;;;### (autoloads nil nil ("amd-mode-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; amd-mode-autoloads.el ends here
